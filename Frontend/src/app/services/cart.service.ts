import { Injectable } from "@angular/core";
import { LocalStorageService } from "ngx-webstorage";
import { Subject } from "rxjs";
import { Cart } from "../models/cart.model";

@Injectable({
    providedIn: 'root'
})
export class CartService {
    cartItemNumber: Subject<number> = new Subject();
    onCheckout: Subject<boolean> = new Subject();

    constructor(
        private localStorage:LocalStorageService) { }
    
    setCart(cart: Cart){
        let totalQty = 0;

        if(cart != null) {
            for (const [key, value] of Object.entries(cart.quantities)){
                totalQty += value;
            }
        }

        this.cartItemNumber.next(totalQty);

        this.localStorage.store('cartId', cart.id);
        this.localStorage.store('cartSize', totalQty);
    }

    clearCart() {
        this.localStorage.clear('cartId');
        this.localStorage.clear('cartSize');
        this.cartItemNumber.next(0);
    }

    getCartId() {
        return this.localStorage.retrieve('cartId');
    }

    getCartItemsNo() {
        return this.localStorage.retrieve('cartSize');
    }
}