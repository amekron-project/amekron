import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { LocalStorageService } from "ngx-webstorage";
import { take } from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class ProfileService {
    retrieveResponse: any;
    base64Data: any;
    
    constructor(private httpClient: HttpClient, private localStorage: LocalStorageService) { }

    public async onRetrieveCustomerImage(customerId){
        //Make a call to Spring Boot to get the Image Bytes.
        return new Promise(resolve => {
            this.httpClient.get('http://localhost:8081/imageCustomer/get/' + customerId)
            .pipe(take(1))
            .subscribe((response: any) => {
                resolve('data:image/jpeg;base64,' + response.picByte);
            },
            (error: HttpErrorResponse) => {
                resolve("../assets/images/avatar.png");
            });
        });
        
    }
}