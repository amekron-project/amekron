import { ElementRef, Injectable, Renderer2 } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { LocalStorageService } from 'ngx-webstorage';
import { Product } from '../models/product.model';
import { Review } from '../models/review.model';

@Injectable({
    providedIn: 'root'
})
export class ProductService {
    private resultById: Product;
    private resultByName: Product[];
    
    constructor(private sanitizer: DomSanitizer, private localStorage:LocalStorageService) { }

    setSelectedProductReviews(reviews: Review[]) {
      this.localStorage.store('pdpProductReviews', reviews);
    }

    setSelectedProduct(product: Product){
      this.localStorage.store('pdpProduct', product);
    }

    getSelectedProduct() {
      return this.localStorage.retrieve('pdpProduct');
    }

    updateResultId(product: Product){
      this.resultById = product;
    }

    updateResultName(products: Product[]){
      this.resultByName = products;
    }

    getResultId(): Product{
      return this.resultById;
    }

    getResultName(): Product[]{
      return this.resultByName;
    }

    calculateHeights(elem: ElementRef, renderer: Renderer2) {
        let max: number[] = [];
        let maxTemp : number = 0;
        let elemNr = 1;
        let maxElemRow;
  
        if(window.innerWidth >= 992)
          maxElemRow = 3;
        else
        if(window.innerWidth < 992 && window.innerWidth >= 576)
          maxElemRow = 2;
        else
          maxElemRow = 1;
  
        elem.nativeElement.querySelectorAll('.product-name').forEach(element => {
          renderer.setStyle(element, 'height', `auto`);
        });
  
        elem.nativeElement.querySelectorAll('.product-name').forEach(element => {
          if(element.offsetHeight > maxTemp)
            maxTemp = element.offsetHeight;
  
          if(elemNr === maxElemRow) {
            max.push(maxTemp);
            elemNr = 0;
            maxTemp = 0;
          }
  
          elemNr++;
        });
  
        if(elemNr < maxElemRow) {
          max.push(maxTemp)
        }
  
        elemNr = -1;
        elem.nativeElement.querySelectorAll('.product-name').forEach(element => {
          ++elemNr;
          renderer.setStyle(element, 'height', `${max[parseInt(`${elemNr/maxElemRow}`)]}px`);
        });
  
    }

    getImgContent(imgFile: string): SafeUrl {
      return this.sanitizer.bypassSecurityTrustUrl(imgFile);
    }
  
}