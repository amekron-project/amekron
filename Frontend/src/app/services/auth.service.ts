import { Injectable, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { LocalStorageService } from 'ngx-webstorage';
import { Subject, Subscription, take } from 'rxjs';
import * as fromApp from '../store/app.reducer';
import * as AuthActions from '../components/login/store/auth.actions'; 
import { CartService } from './cart.service';
import { JwtResponse } from '../models/jwt-response.model';

@Injectable({providedIn: 'root'})
export class AuthService implements OnDestroy{
    loggedIn = new Subject<boolean>();
    promiseLoggedIn = false;
    storeSub: Subscription;

    constructor(
        private cartService: CartService, 
        private store: Store<fromApp.AppState>, 
        private localStorage: LocalStorageService){

            this.storeSub = this.store.select('auth').subscribe(authState => {
                if(authState.jwtResponse == null || !this.isUserSignedin()){
                    this.loggedIn.next(false);
                    this.loggedIn
                    .pipe(take(1))
                    .subscribe(val => 
                        this.promiseLoggedIn = val
                    )
                }
                else {
                    this.loggedIn.next(true); 
                    this.loggedIn
                    .pipe(take(1))
                    .subscribe(val => 
                        this.promiseLoggedIn = val
                    )
                }
            })
    }

    ngOnDestroy(): void {
        this.storeSub.unsubscribe();
    }
  
    isAuthenticated(){
      if(this.getSignedinUser() != null)
        this.promiseLoggedIn = true;
        const promise = new Promise(
            (resolve,reject) => {
                resolve(this.promiseLoggedIn);
            }
        )

      return promise;
    }

    storeHTTPToken(HTTPToken: string) {
        this.localStorage.store('token', 'HTTP_TOKEN ' + HTTPToken);
    }

    storeUpdatedCustomerData(customer: JwtResponse) {
        this.localStorage.store('customer', customer);
    }

    isUserSignedin() {
		return this.localStorage.retrieve('token') !== null;
	}

	getSignedinUser() {
		return this.localStorage.retrieve('customer')?.customer;
	}

	getHTTPToken() {
		return this.localStorage.retrieve('token') as string;
	}

    getToken() {
		return this.localStorage.retrieve('customer')?.token as string;
	}

    getRoles() {
        return this.localStorage.retrieve('customer')?.roles;
    }

    clearCustomerData() {
        this.localStorage.clear('customer');
        this.localStorage.clear('token');
    }

    onLogout(){
        this.store.dispatch(new AuthActions.Logout());
        this.loggedIn.next(false);
        this.cartService.cartItemNumber.next(0);
    }
}
  