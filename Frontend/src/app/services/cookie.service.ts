import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';


@Injectable({
    providedIn: 'root'
})
export class CookieLoginService {

    constructor(public cookieService: CookieService) { }

    rememberMe(email: string, password: string) {
        this.cookieService.set("cookieEmail", email);
        this.cookieService.set("cookiePassword", password);
    }
}
