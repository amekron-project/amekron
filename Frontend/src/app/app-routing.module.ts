import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ProductSearchComponent } from './components/catalogue/product-search/product-search.component';
import { AuthGuard } from './services/auth-guard.service';
import { ProfileGuard } from './services/profile-guard.service';
import { EmailSentComponent } from './components/email-sent/email-sent.component';
import { EmailVerifiedComponent } from './components/email-verified/email-verified.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./components/home/home.module').then(mod => mod.HomeModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./components/home/home.module').then(mod => mod.HomeModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./components/login/login.module').then(mod => mod.LoginModule),
    canActivateChild: [AuthGuard]
  },
  {
    path: 'my-account',
    loadChildren: () => import('./components/profile-page/profile-page.module').then(mod => mod.ProfilePageModule),
    canActivateChild: [ProfileGuard]
  },
  {
    path: 'catalogue',
    loadChildren: () => import('./components/catalogue/catalogue.module').then(mod => mod.CatalogueModule)
  },
  {
    path: 'cart',
    loadChildren: () => import('./components/cart/cart.module').then(mod => mod.CartModule)
  },
  { path: 'catalogue_search_results', component: ProductSearchComponent },
  { path: 'not-found', component: PageNotFoundComponent},
  { path: 'email-sent', component: EmailSentComponent},
  { path: 'email-verified/:confirmation-token', component: EmailVerifiedComponent},
  { path: '**', redirectTo: '/not-found'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
