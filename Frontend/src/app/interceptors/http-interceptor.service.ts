import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, catchError, throwError } from "rxjs";
import { AuthService } from "../services/auth.service";

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {
 
    constructor(private authService: AuthService) { }
 
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.authService.isUserSignedin() && this.authService.getHTTPToken()) {
            const request = req.clone({
                headers: new HttpHeaders({
                    'Authorization': this.authService.getHTTPToken()
                })
            });
            return next.handle(request).pipe(
      
				catchError(err => {
					if(err instanceof HttpErrorResponse && err.status === 401) {
						this.authService.onLogout();
                        console.log("Token revoked");
					}
					return throwError(err);
				})
			);
        }
       
		return next.handle(req);
    }

}