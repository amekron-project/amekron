
import { ActionReducerMap } from '@ngrx/store';
import * as fromAuth from '../components/login/store/auth.reducer';
import * as fromProfile from '../components/profile-page/store/profile.reducer';
import * as fromPlp from '../components/catalogue/plp/store/plp.reducer';
import * as fromPdp from '../components/catalogue/pdp/store/pdp.reducer';
import * as fromCart from '../components/cart/store/cart.reducer';
import * as fromSearch from '../components/searchbar-product/store/search.reducer';

export interface AppState{
    auth: fromAuth.State,
    profile: fromProfile.State,
    plp: fromPlp.State,
    pdp: fromPdp.State,
    search: fromSearch.State,
    cart: fromCart.State
}

export const appReducer: ActionReducerMap<AppState> = {
    auth: fromAuth.authReducer,
    profile: fromProfile.profileReducer,
    plp: fromPlp.plpReducer,
    pdp: fromPdp.pdpReducer,
    search: fromSearch.searchReducer,
    cart: fromCart.cartReducer
};
