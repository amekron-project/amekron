import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeModule } from './components/home/home.module';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MatBadgeModule} from '@angular/material/badge';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { ProfilePageModule } from './components/profile-page/profile-page.module';
import { FooterSharedModule} from './components/footer-shared-module/footer-shared-module.module';
import { StoreModule } from '@ngrx/store';
import * as fromApp from './store/app.reducer';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects } from './components/login/store/auth.effects';
import { ProfileEffects } from './components/profile-page/store/profile.effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from 'src/environments/environment';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { PlpEffects } from './components/catalogue/plp/store/plp.effects';
import { SearchbarProductComponent } from './components/searchbar-product/searchbar-product.component';
import { SearchEffects } from './components/searchbar-product/store/search.effects';
import { FormsModule } from '@angular/forms';
import { PdpEffects } from './components/catalogue/pdp/store/pdp.effects';
import { CatalogueModule } from './components/catalogue/catalogue.module';
import { CartModule } from './components/cart/cart.module';
import { CartEffects } from './components/cart/store/cart.effects';
import { HttpInterceptorService } from './interceptors/http-interceptor.service';
import { EmailSentComponent } from './components/email-sent/email-sent.component';
import { EmailVerifiedComponent } from './components/email-verified/email-verified.component';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    SearchbarProductComponent,
    EmailSentComponent,
    EmailVerifiedComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    CatalogueModule,
    CartModule,
    HomeModule,
    MatBadgeModule,
    MatIconModule,
    NgbModule,
    ProfilePageModule,
    FooterSharedModule,
    FormsModule,
    NgxWebstorageModule.forRoot(),
    StoreModule.forRoot(fromApp.appReducer),
    EffectsModule.forRoot([AuthEffects, ProfileEffects, PlpEffects, PdpEffects, SearchEffects, CartEffects]),
    StoreDevtoolsModule.instrument({logOnly: environment.production})
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
