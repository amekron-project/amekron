
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { Store } from '@ngrx/store';
import { CartService } from 'src/app/services/cart.service';
import { Customer } from '../../../models/customer.model';
import { CookieLoginService } from '../../../services/cookie.service';
import * as fromApp from '../../../store/app.reducer';
import * as AuthActions from '../store/auth.actions';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  registerForm: FormGroup = new FormGroup({});

  checked = false;

  hide = true;
  loginForm: FormGroup = new FormGroup({});
  emailLoginCookie = '';
  passwordLoginCookie = '';

  constructor(
    private fb: FormBuilder,
    private cookieLoginService: CookieLoginService,
    private store: Store<fromApp.AppState>
  ) {
     if (this.cookieLoginService.cookieService.check("cookieEmail"))
       this.checked = true;
     this.emailLoginCookie = cookieLoginService.cookieService.get('cookieEmail');
     this.passwordLoginCookie = cookieLoginService.cookieService.get('cookiePassword');
  }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: [this.emailLoginCookie, Validators.compose(
        [Validators.required]
      )],

      password: [this.passwordLoginCookie,
      {
        validators: [
          Validators.compose([
            Validators.required
          ])
        ],
        updateOn: 'blur'
      }],

    })
  }

  onClickRememberMe(checkbox: MatCheckboxChange) {
     if (checkbox.checked)
     {
       this.cookieLoginService.rememberMe(this.emailLogin?.value, this.passwordLogin?.value);
       this.checked = checkbox.checked;
     }
     else {
       this.cookieLoginService.cookieService.deleteAll();
       this.checked = checkbox.checked;
     }
  }

  async onLogin(lForm: { value: Customer; reset: () => void; }) {
    this.store.dispatch(new AuthActions.Login(lForm.value));
    if (this.checked)
      this.cookieLoginService.rememberMe(lForm.value.email, lForm.value.password);
  }


  get emailLogin() {
    return this.loginForm.get("email");
  }

  get passwordLogin() {
    return this.loginForm.get("password");
  }

  getErrorMessageRequiredEmailLogin() {
    return this.loginForm.get("email")?.hasError('required') ? 'You must enter your email' : true;
  }

  getErrorMessageRequiredPasswordLogin() {
    return this.passwordLogin?.hasError('required') ? 'You must enter your password' : true;
  }

}
