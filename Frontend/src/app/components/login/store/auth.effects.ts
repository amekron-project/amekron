import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Store } from "@ngrx/store";
import { catchError, of, switchMap, tap, withLatestFrom } from "rxjs";
import { map } from "rxjs";
import { Customer } from "src/app/models/customer.model";
import { environment } from "src/environments/environment";
import * as AuthActions from './auth.actions';
import * as fromApp from '../../../store/app.reducer';
import { Router } from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import { MatSnackBar } from "@angular/material/snack-bar";
import { CartService } from "src/app/services/cart.service";
import { JwtResponse } from "src/app/models/jwt-response.model";
import { AuthService } from "src/app/services/auth.service";
import { SpinnerService } from "src/app/services/spinner.service";


@Injectable()
export class AuthEffects {
    constructor(
        private actions$: Actions,
        private http: HttpClient,
        private router: Router,
        public cookieService: CookieService,
        private cartService: CartService,
        private authService: AuthService,
        private spinnerService: SpinnerService,
        public snackBar: MatSnackBar,
        private store: Store<fromApp.AppState>
    ){}

    @Effect()
    authLogin = this.actions$.pipe(
        ofType(AuthActions.LOGIN),
        withLatestFrom(this.store.select('auth')),
        switchMap(([actionData, authState]) => {
            return this.http.post<JwtResponse>(`${environment.apiBaseUrl}/customer/signin`, authState.loginData)
            .pipe(
                tap(resData => {
                    this.cartService.clearCart();
                    this.authService.storeHTTPToken(resData.token);
                    this.authService.storeUpdatedCustomerData(resData);
                    this.router.navigate(['/home']);
                }),
                map(resData => {return new AuthActions.LoginSuccess(resData);}),
                catchError(errorRes => {
                    this.snackBar.open("No user found with this email and password!", 'Close');
                    return of(new AuthActions.LoginFail())
                })
            )
        })
    )

    @Effect({dispatch: false})
    authLogout = this.actions$.pipe(
        ofType(AuthActions.LOGOUT),
        withLatestFrom(this.store.select('auth')),
        map(resData => {
            this.cartService.clearCart();
            this.authService.clearCustomerData();
            this.router.navigate(['/home']);
        })
    )

    @Effect()
    registerCustomer = this.actions$.pipe(
        ofType(AuthActions.REGISTER),
        withLatestFrom(this.store.select('auth')),
        switchMap(([actionData, authState]) => {
            this.spinnerService.showSpinner.next(true);
            return this.http.post<JwtResponse>(`${environment.apiBaseUrl}/customer/signup`, authState.jwtRequest)
            .pipe(
                map(resData => {
                    this.spinnerService.showSpinner.next(false);
                    this.cartService.clearCart();
                    this.authService.storeHTTPToken(resData.token);
                    this.authService.storeUpdatedCustomerData(resData);
                    this.router.navigate(['/email-sent'])
                    return new AuthActions.RegisterSuccess(resData);
                }),
                catchError(errorRes => {
                    this.snackBar.open("We could not register you. Another user with this email may exist already. Please contact us at ### otherwise!", 'Close');
                    this.spinnerService.showSpinner.next(false);
                    return of(new AuthActions.RegisterFail())
                })
            )
        })
    )

    @Effect()
    confirmEmailCustomer = this.actions$.pipe(
        ofType(AuthActions.CONFIRM_EMAIL),
        withLatestFrom(this.store.select('auth')),
        switchMap(([actionData, authState]) => {
            return this.http.post(`${environment.apiBaseUrl}/customer/confirm-account`, authState.confirmationToken)
            .pipe(
                map(resData => {
                    return new AuthActions.ConfirmEmailSuccess();
                }),
                catchError(errorRes => {
                    this.snackBar.open("Your email could not be confirmed. Please contact us at ###!", 'Close');
                    return of(new AuthActions.ConfirmEmailFail())
                })
            )
        })
    )
}
