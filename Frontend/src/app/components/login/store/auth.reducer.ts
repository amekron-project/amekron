import { Customer } from 'src/app/models/customer.model';
import { JwtRequest } from 'src/app/models/jwt-request.model';
import { JwtResponse } from 'src/app/models/jwt-response.model';
import * as AuthActions from './auth.actions';

export interface State {
    confirmedEmail: boolean,
    confirmationToken: String,
    loginData: Customer,
    jwtResponse: JwtResponse,//response from sign in (with token)
    jwtRequest: JwtRequest
}

const initialState: State = {
    confirmedEmail: false,
    confirmationToken: null,
    loginData: null,
    jwtResponse: null,
    jwtRequest: null
} 

export function authReducer(state = initialState, action: AuthActions.AuthActions){
    switch (action.type){
        case AuthActions.LOGIN:
            const loginData = action.payload;

            return {
                ...state,
                loginData: loginData
            }

        case AuthActions.LOGIN_SUCCESS: 
            const jwtResponse = action.payload;
            return {
                ...state,
                jwtResponse: jwtResponse
            };

        case AuthActions.LOGIN_FAIL:
            return {
                ...state,
                JwtResponse: null
            };


        case AuthActions.REGISTER:
            const registerData = action.payload;
            return {
                ...state,
                jwtRequest: registerData,
            };
        
        case AuthActions.REGISTER_FAIL:
            return {
                ...state,
                jwtResponse: null
            };

        case AuthActions.REGISTER_SUCCESS:
            const registerResponse = action.payload;
            return {
                ...state,
                jwtResponse: registerResponse
            };

        case AuthActions.LOGOUT:
            return {
                ...state,
                jwtResponse: null
            };

        case AuthActions.CONFIRM_EMAIL:
            const confirmationToken = action.payload;
            return {
                ...state,
                confirmationToken: confirmationToken
            }

        case AuthActions.CONFIRM_EMAIL_FAIL:
            return {
                ...state,
                confirmedEmail: false
            }
        
        case AuthActions.CONFIRM_EMAIL_SUCCESS:
            return {
                ...state, 
                confirmedEmail: true
            }
            
        default:
            return state;
    }
}