import { Action } from "@ngrx/store";
import { Customer } from "src/app/models/customer.model";
import { JwtRequest } from "src/app/models/jwt-request.model";
import { JwtResponse } from "src/app/models/jwt-response.model";

export const LOGIN = '[Auth] LOGIN';
export const LOGIN_FAIL = '[Auth] LOGIN_FAIL';
export const LOGIN_SUCCESS = '[Auth] LOGIN_SUCCESS';
export const LOGOUT = '[Auth] LOGOUT';
export const REGISTER = '[Auth] REGISTER';
export const REGISTER_FAIL = '[Register] REGISTER_FAIL';
export const REGISTER_SUCCESS = '[Register] REGISTER_SUCCESS';
export const CONFIRM_EMAIL = '[Register] CONFIRM_EMAIL';
export const CONFIRM_EMAIL_SUCCESS = '[Register] CONFIRM_EMAIL_SUCCESS';
export const CONFIRM_EMAIL_FAIL = '[Register] CONFIRM_EMAIL_FAIL';

export class Login implements Action {
    readonly type = LOGIN;

    constructor(public payload: Customer){}
}

export class LoginFail implements Action {
    readonly type = LOGIN_FAIL;
}

export class LoginSuccess implements Action {
    readonly type = LOGIN_SUCCESS;

    constructor(public payload: JwtResponse){}
}

export class Logout implements Action {
    readonly type = LOGOUT;
}

export class Register implements Action {
    readonly type = REGISTER;

    constructor(public payload: JwtRequest){}
}

export class RegisterFail implements Action {
    readonly type = REGISTER_FAIL;
}

export class RegisterSuccess implements Action {
    readonly type = REGISTER_SUCCESS;

    constructor(public payload: JwtResponse){}
}

export class ConfirmEmail implements Action {
    readonly type = CONFIRM_EMAIL;

    constructor(public payload: String){}
}

export class ConfirmEmailSuccess implements Action {
    readonly type = CONFIRM_EMAIL_SUCCESS;
}

export class ConfirmEmailFail implements Action {
    readonly type = CONFIRM_EMAIL_FAIL;
}

export type AuthActions = 
| Login 
| Logout
| LoginFail
| LoginSuccess
| Register
| RegisterSuccess
| RegisterFail
| ConfirmEmail
| ConfirmEmailFail
| ConfirmEmailSuccess;