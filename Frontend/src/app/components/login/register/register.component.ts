import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Customer } from 'src/app/models/customer.model';
import { JwtRequest } from 'src/app/models/jwt-request.model';
import { SpinnerService } from 'src/app/services/spinner.service';
import { CustomValidators } from 'src/app/utils/custom-validators';
import * as fromApp from '../../../store/app.reducer';
import * as AuthActions from '../store/auth.actions';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.less']
})
export class RegisterComponent implements OnInit {
  hide1 = true;
  hide2 = true;
  registerForm: FormGroup = new FormGroup({});
  showSpinner: boolean = false;

  constructor(
    private spinnerService: SpinnerService,
    private fb: FormBuilder,
    private store: Store<fromApp.AppState>,
    ) { }

  ngOnInit(): void {
    this.spinnerService.showSpinner.subscribe(res => this.showSpinner = res);

    this.registerForm = this.fb.group({

      email: ['', Validators.compose(
        [Validators.required]
      )],

      firstName: ['', Validators.compose(
        [Validators.required]
      )],

      lastName: ['', Validators.compose(
        [Validators.required]
      )],

      password: ['',
        {
          validators: [
            Validators.compose([
              Validators.required,
              Validators.minLength(6),
              CustomValidators.passwordStrength()
            ])
          ],
          updateOn: 'blur'
        }],

      passwordRegisterConfirm: ['',
        {
          validators: [
            Validators.compose([
              Validators.required
            ])
          ],
          updateOn: 'blur'
        }],

        address : [''],
        city: [''],
        country: [''],
        zipCode: [''],
        phone: [''],
    })
  }

  async onRegister(rForm: { value: Customer; reset: () => void; }) {
    let jwtRequest: JwtRequest = new JwtRequest(
      rForm.value.firstName,
      rForm.value.lastName,
      rForm.value.email,
      rForm.value.password,
      rForm.value.address,
      rForm.value.city,
      rForm.value.country,
      rForm.value.zipCode,
      rForm.value.phone,
      ["ROLE_USER"]
    )
    this.store.dispatch(new AuthActions.Register(jwtRequest));
  }

  get address() {
    return this.registerForm.get("address");
  }

  get city() {
    return this.registerForm.get("city");
  }

  get country() {
    return this.registerForm.get("country");
  }

  get zipCode() {
    return this.registerForm.get("zipCode");
  }

  get firstName() {
    return this.registerForm.get("firstName");
  }

  get lastName() {
    return this.registerForm.get("lastName");
  }

  get email() {
    return this.registerForm.get("email");
  }

  get password() {
    return this.registerForm.get("password");
  }

  get passwordRegisterConfirm() {
    return this.registerForm.get("passwordRegisterConfirm");
  }

  getErrorMessageRequiredFirstName() {
    return this.firstName?.hasError('required') ? 'You must enter your first name' : true;
  }

  getErrorMessageLengthFirstName() {
    return this.firstName?.errors?.['hasMaximumValue'] == null ? 'Your name must be maximum 10 characters long' : true;
  }

  getErrorMessageRequiredLastName() {
    return this.lastName?.hasError('required') ? 'You must enter your last name' : true;
  }

  getErrorMessageLengthLastName() {
    return this.lastName?.errors?.['hasMaximumValue'] == null ? 'Your name must be maximum 20 characters long' : true;
  }

  getErrorMessageRequiredEmailRegister() {
    return this.registerForm.get("email")?.hasError('required') ? 'You must enter an email' : true;
  }

  getErrorMessageRequiredPasswordRegister() {
    return this.password?.hasError('required') ? 'You must enter a password' : true;
  }

  getErrorMessageStrengthPasswordRegister() {
    return this.password?.errors?.['passwordStrength'] ? 'Your password must contain at least one capital letter, one small letter, one digit and one special character' : true;
  }

  getErrorMessageLengthPasswordRegister() {
    return this.password?.hasError('minlength') ? 'Your password must be at least 6 characters long' : true;
  }

  getErrorMessageRequiredPasswordRegisterConfirm() {
    return this.passwordRegisterConfirm?.hasError('required') ? 'You must enter the password typed above' : true;
  }

  getErrorPasswordsEqual(){
    const matched: boolean = this.password?.value === this.passwordRegisterConfirm?.value;

    if (matched) {
      this.registerForm.controls['passwordRegisterConfirm'].setErrors(null);
    } else {
      this.registerForm.controls['passwordRegisterConfirm'].setErrors({
        notMatched: true
      });
    }

    return matched;
  }

}
