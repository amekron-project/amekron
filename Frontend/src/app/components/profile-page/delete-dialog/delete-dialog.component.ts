import { HttpClient } from '@angular/common/http';
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { take } from 'rxjs';
import { Customer } from 'src/app/models/customer.model';
import * as fromApp from '../../../store/app.reducer';
import * as ProfileActions from '../store/profile.actions';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.less']
})
export class DeleteDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DeleteDialogComponent>,
    private store: Store<fromApp.AppState>,
    private httpClient: HttpClient,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  async onDelete(updateForm: { value: Customer; reset: () => void; }) {
    this.httpClient.delete('http://localhost:8081/imageCustomer/delete/' + updateForm.value.id).subscribe(() => {
      take(1)
    })
    this.store.dispatch(new ProfileActions.Delete(updateForm.value));

    this.onNoClick();
  }

}
