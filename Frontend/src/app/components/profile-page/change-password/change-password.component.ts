import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { LocalStorageService } from 'ngx-webstorage';
import { Customer } from 'src/app/models/customer.model';
import { CustomValidators } from 'src/app/utils/custom-validators';
import { Store } from '@ngrx/store';
import * as fromApp from '../../../store/app.reducer'; 
import * as ProfileActions from '../store/profile.actions';
import { Subscription } from 'rxjs';
import { MessageDialogComponent } from '../../../utils/message-dialog/message-dialog.component';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.less']
})
export class ChangePasswordComponent implements OnInit, OnDestroy {
  customer: Customer;
  hide1 = true;
  hide2 = true;
  storeSub: Subscription;
  changePasswordForm: FormGroup = new FormGroup({});

  constructor(
    private fb: FormBuilder,
    private dialog: MatDialog,
    private authService: AuthService,
    private store: Store<fromApp.AppState> 
    ) { }

  ngOnDestroy(): void {
    this.storeSub.unsubscribe();
  }

  ngOnInit(): void {
    this.customer = this.authService.getSignedinUser();
    
    this.changePasswordForm = this.fb.group({
      id: [this.customer.id],
      firstName: [this.customer.firstName],
      lastName: [this.customer.lastName],
      email: [this.customer.email],
      address : [this.customer.address],
      city: [this.customer.city],
      country: [this.customer.country],
      zipCode: [this.customer.zipCode],
      phone: [this.customer.phone],

      password: ['',
        {
          validators: [
            Validators.compose([
              Validators.required,
              Validators.minLength(6),
              CustomValidators.passwordStrength()
            ])
          ],
          updateOn: 'blur'
        }],

      passwordConfirm: ['',
        {
          validators: [
            Validators.compose([
              Validators.required
            ])
          ],
          updateOn: 'blur'
        }]
    });

    this.storeSub = this.store.select('profile').subscribe(profileState => {
      if(profileState.updateSuccess === true){
        this.dialog.open(MessageDialogComponent, {
            width: '500px',
            data: {message: '<div>Account successfully updated!</div>'}
        });
      }
    })

  }

  onChangePassword(form: { value: Customer; reset: () => void; }) {
    this.store.dispatch(new ProfileActions.Update(form.value));
  }

  get password() {
    return this.changePasswordForm.get("password");
  }

  get passwordConfirm() {
    return this.changePasswordForm.get("passwordConfirm");
  }

  getErrorMessageRequiredPassword() {
    return this.password?.hasError('required') ? 'You must enter a password' : true;
  }

  getErrorMessageStrengthPassword() {
    return this.password?.errors?.['passwordStrength'] ? 'Your password must contain at least one capital letter, one small letter, one digit and one special character' : true;
  }

  getErrorMessageLengthPassword() {
    return this.password?.hasError('minlength') ? 'Your password must be at least 6 characters long' : true;
  }

  getErrorMessageRequiredPasswordConfirm() {
    return this.passwordConfirm?.hasError('required') ? 'You must enter the password typed above' : true;
  }

  getErrorPasswordsEqual(){
    const matched: boolean = this.password?.value === this.passwordConfirm?.value;
    if (matched) {
      this.changePasswordForm.controls['passwordConfirm'].setErrors(null);
    } else {
      this.changePasswordForm.controls['passwordConfirm'].setErrors({
        notMatched: true
      });
    }

    return matched;
  }
  
}

