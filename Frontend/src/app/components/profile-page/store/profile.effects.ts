import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Store } from "@ngrx/store";
import { LocalStorageService } from "ngx-webstorage";
import * as ProfileActions from './profile.actions';
import * as fromApp from '../../../store/app.reducer';
import * as AuthActions from '../../login/store/auth.actions';
import { catchError, map, of, switchMap, tap, withLatestFrom } from "rxjs";
import { environment } from "src/environments/environment";
import { Customer } from "src/app/models/customer.model";
import { AuthService } from "src/app/services/auth.service";
import { JwtResponse } from "src/app/models/jwt-response.model";
import { SpinnerService } from "src/app/services/spinner.service";

@Injectable()
export class ProfileEffects {
    constructor(
        private actions$: Actions,
        private authService: AuthService,
        private localStorage: LocalStorageService,
        private spinnerService: SpinnerService,
        private http: HttpClient,
        public snackBar: MatSnackBar,
        private store: Store<fromApp.AppState>
    ){}


    @Effect()
    profileUpdate = this.actions$.pipe(
        ofType(ProfileActions.UPDATE),
        withLatestFrom(this.store.select('profile')),
        switchMap(([actionData, profileState]) => {
            return this.http.put<Customer>(`${environment.apiBaseUrl}/customer/update`, profileState.customer)
            .pipe(
                map(resData => {
                    let newJwtResponse: JwtResponse = new JwtResponse(
                        resData,
                        this.authService.getToken(),
                        this.authService.getRoles()
                    );
                    this.authService.storeUpdatedCustomerData(newJwtResponse);
                    return new ProfileActions.UpdateSuccess(resData);
                }),
                catchError(errorRes => {
                    this.snackBar.open("Ops! We could not update your information. Please contact us at ###", 'Close');
                    return of(new ProfileActions.UpdateFail())
                })
            )
        })
    )

    @Effect()
    profileDeleteSuccess = this.actions$.pipe(
        ofType(ProfileActions.DELETE_SUCCESS),
        map(resData => {
            this.authService.clearCustomerData();
            return new AuthActions.Logout();
        })
    )


    @Effect()
    profileDelete = this.actions$.pipe(
        ofType(ProfileActions.DELETE),
        withLatestFrom(this.store.select('profile')),
        switchMap(([actionData, profileState]) => {
            this.spinnerService.showSpinner.next(true);

            return this.http.delete<void>(`${environment.apiBaseUrl}/customer/delete/${profileState.customer.id}`)
            .pipe(
                map(resData => {
                    this.spinnerService.showSpinner.next(false);
                    return new ProfileActions.DeleteSuccess();
                }),
                catchError(errorRes => {
                    this.snackBar.open("Ops! We could not delete your account. Please contact us at ###", 'Close');
                    return of(new ProfileActions.DeleteFail())
                })
            )
        })
    )

    @Effect()
    getById = this.actions$.pipe(
        ofType(ProfileActions.GET_BY_ID),
        withLatestFrom(this.store.select('profile')),
        switchMap(([actionData, profileState]) => {
            return this.http.get<Customer>(`${environment.apiBaseUrl}/customer/findById/${profileState.id}`)
            .pipe(
                tap(resData => {
                    let newJwtResponse: JwtResponse = new JwtResponse(
                        resData,
                        this.authService.getToken(),
                        this.authService.getRoles()
                    );
                    this.localStorage.store('customer', newJwtResponse);
                }),
                map(resData => {return new ProfileActions.GetByIdSucces(resData);}),
                catchError(errorRes => {
                    return of(new ProfileActions.GetByIdFail())
                })
            )
        })
    )

    @Effect()
    changePassword = this.actions$.pipe(
        ofType(ProfileActions.CHANGE_PASSWORD),
        withLatestFrom(this.store.select('profile')),
        switchMap(([actionData, profileState]) => {
            return this.http.post<Customer>(`${environment.apiBaseUrl}/customer/change-password`, profileState.customer)
            .pipe(
                map(resData => {return new ProfileActions.ChangePasswordSuccess()}),
                catchError(errorRes => {
                    this.snackBar.open("Something went wrong and we cannot forward you to change your password! Please contact us at ###", 'Close');
                    return of(new ProfileActions.ChangePasswordFail())
                })
            )
        })
    )
}
