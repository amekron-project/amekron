import { Customer } from "src/app/models/customer.model";
import * as ProfileActions from './profile.actions';

export interface State {
    customer: Customer;
    updateSuccess: boolean;
    deleteSuccess: boolean;
    id: number;
    customerById: Customer;
    getByIdSuccess: boolean;
    changePasswordSuccess: boolean;
}

const initialState: State = {
    customer: null,
    updateSuccess: false,
    deleteSuccess: false,
    id: null,
    customerById: null,
    getByIdSuccess: false,
    changePasswordSuccess: false
}

export function profileReducer(state = initialState, action: ProfileActions.ProfileActions){
    switch (action.type){
        case ProfileActions.UPDATE:
            const customer = action.payload;
            return {
                ...state,
                customer: customer,
                updateSuccess: false
            }

        case ProfileActions.UPDATE_SUCCESS:
            const customerSuccess = action.payload;
            return {
                ...state,
                customer: customerSuccess,
                updateSuccess: true
            }

        case ProfileActions.UPDATE_FAIL:
            return {
                ...state,
                updateSuccess: false
            }

        case ProfileActions.DELETE:
            const customerDelete = action.payload;
            return {
                ...state,
                deleteSuccess: true,
                customer: customerDelete
            }

        case ProfileActions.DELETE_SUCCESS:
            return {
                ...state,
                deleteSuccess: true,
                customer: null
            }

        case ProfileActions.DELETE_FAIL:
            return {
                ...state,
                deleteSuccess: false
            }

        case ProfileActions.GET_BY_ID:
            const id = action.payload;
            return {
                ...state,
                id: id,
                getByIdSuccess: false
            }

        case ProfileActions.GET_BY_ID_SUCCESS:
            const customerById = action.payload;
            return {
                ...state,
                customerById: customerById,
                getByIdSuccess: true
            }

        case ProfileActions.GET_BY_ID_FAIL:
            return {
                ...state,
                getByIdSuccess: false
            }

        case ProfileActions.CHANGE_PASSWORD:
            const customerChangePassword = action.payload;
            return {
                ...state,
                customer: customerChangePassword
            }
        
        case ProfileActions.CHANGE_PASSWORD_SUCCESS:
            return {
                ...state,
                changePasswordSuccess: true
            }

        case ProfileActions.CHANGE_PASSWORD_FAIL:
            return {
                ...state,
                changePasswordSuccess: false
            }

        default:
            return state;
    }
}
