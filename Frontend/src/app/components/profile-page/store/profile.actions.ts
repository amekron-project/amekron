import { Action } from "@ngrx/store";
import { Customer } from "src/app/models/customer.model";

export const UPDATE = '[Profile] UPDATE';
export const UPDATE_SUCCESS = '[Profile] UPDATE_SUCCESS';
export const UPDATE_FAIL = '[Profile] UPDATE_FAIL';
export const DELETE = '[Profile] DELETE';
export const DELETE_SUCCESS = '[Profile] DELETE_SUCCESS';
export const DELETE_FAIL = '[Profile] DELETE_FAIL';
export const GET_BY_ID = '[Profile] GET_BY_ID';
export const GET_BY_ID_SUCCESS = '[Profile] GET_BY_ID_SUCCESS';
export const GET_BY_ID_FAIL = '[Profile] GET_BY_ID_FAIL';
export const CHANGE_PASSWORD = '[Profile] CHANGE_PASSWORD';
export const CHANGE_PASSWORD_SUCCESS = '[Profile] CHANGE_PASSWORD_SUCCESS';
export const CHANGE_PASSWORD_FAIL = '[Profile] CHANGE_PASSWORD_FAIL';

export class Update implements Action {
    readonly type = UPDATE;

    constructor(public payload: Customer){}
}

export class UpdateSuccess implements Action {
    readonly type = UPDATE_SUCCESS;

    constructor(public payload: Customer){}
}

export class UpdateFail implements Action {
    readonly type = UPDATE_FAIL;
}


export class Delete implements Action {
    readonly type = DELETE;

    constructor(public payload: Customer){}
}

export class DeleteSuccess implements Action {
    readonly type = DELETE_SUCCESS;
}

export class DeleteFail implements Action {
    readonly type = DELETE_FAIL;
}

export class GetById implements Action {
    readonly type = GET_BY_ID;

    constructor(public payload: number){}
}

export class GetByIdSucces implements Action {
    readonly type = GET_BY_ID_SUCCESS;

    constructor(public payload: Customer){}
}

export class GetByIdFail implements Action {
    readonly type = GET_BY_ID_FAIL;
}

export class ChangePassword implements Action {
    readonly type = CHANGE_PASSWORD;

    constructor(public payload: Customer){}
}

export class ChangePasswordSuccess implements Action {
    readonly type = CHANGE_PASSWORD_SUCCESS;
}

export class ChangePasswordFail implements Action {
    readonly type = CHANGE_PASSWORD_FAIL;
}

export type ProfileActions =
| Update
| UpdateSuccess
| UpdateFail
| Delete
| DeleteSuccess
| DeleteFail
| GetById
| GetByIdSucces
| GetByIdFail
| ChangePassword
| ChangePasswordFail
| ChangePasswordSuccess;
