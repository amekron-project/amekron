import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar, _SnackBarContainer } from '@angular/material/snack-bar';
import { Customer } from '../../../models/customer.model';
import { Store } from '@ngrx/store';
import * as fromApp from '../../../store/app.reducer';
import * as ProfileActions from '../store/profile.actions';
import { BehaviorSubject, Subject, Subscription, take  } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { DeleteDialogComponent } from '../delete-dialog/delete-dialog.component';
import { AuthService } from 'src/app/services/auth.service';
import { JwtResponse } from 'src/app/models/jwt-response.model';
import { SpinnerService } from 'src/app/services/spinner.service';
import { MessageDialogComponent } from 'src/app/utils/message-dialog/message-dialog.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.less']
})
export class ProfileComponent implements OnInit, OnDestroy {
  customer: Customer;
  updateForm: FormGroup = new FormGroup({});
  selectedFile!: File;
  retrievedImage: any;
  message!: string;
  storeUpdateSub: Subscription;
  storeGetByIdSub: Subscription;
  storeChangePasswordSub: Subscription;
  showSpinner = false;

  update$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  changePassword$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    private authService: AuthService,
    private sanitizer: DomSanitizer,
    private fb: FormBuilder,
    private _snackBar: MatSnackBar,
    private dialog: MatDialog,
    private httpClient: HttpClient,
    private spinnerService: SpinnerService,
    private store: Store<fromApp.AppState>) {}

  ngOnInit(): void {
    this.spinnerService.showSpinner.subscribe(res => this.showSpinner = res);

    this.customer = this.authService.getSignedinUser();

    this.getCustomerDecompressedImage();
    this.getImgContent();

    this.updateForm = this.fb.group({
      id: [this.customer.id],
      firstName: [this.customer.firstName, Validators.required],
      lastName: [this.customer.lastName, Validators.required],
      email: [this.customer.email],
      password: [this.customer.password],
      address : [this.customer.address],
      city: [this.customer.city],
      country: [this.customer.country],
      zipCode: [this.customer.zipCode],
      phone: [this.customer.phone]
    })
  }

  onChangePassword(){
    this.update$.next(false);
    this.changePassword$.next(true);

    this.store.dispatch(new ProfileActions.ChangePassword(this.customer));
    this.storeChangePasswordSub = this.store.select('profile').subscribe(profileState => {

      this.update$.pipe(take(1)).subscribe(update => {

        this.changePassword$.pipe(take(1)).subscribe(changePassword => {

          if(profileState.changePasswordSuccess && !update && changePassword){
            this.dialog.open(MessageDialogComponent, {
              width: '500px',
              data: {message: '<div class="mb-5">We sent you a mail to change your password!</div> <div>Check your inbox :) </div>'}
            });

            this.changePassword$.next(false);
          }

        })
      })

    });
  }

  async onFileSelected(event: any){
    this.selectedFile = <File>event?.target.files[0];
    const uploadImageData = new FormData();
    uploadImageData.append('imageFile', this.selectedFile, this.selectedFile.name);

    this.httpClient.post('http://localhost:8081/imageCustomer/upload/' + this.customer.id, uploadImageData)
      .pipe(take(1))
      .subscribe((customerResponse: Customer) => {
        this.customer = customerResponse;

        let newJwtResponse: JwtResponse = new JwtResponse(
          this.customer,
          this.authService.getToken(),
          this.authService.getRoles()
        );

        this.authService.storeUpdatedCustomerData(newJwtResponse);
      },
      (error: HttpErrorResponse) => {
        var message = "Ops! We could not update your profile photo. Your photo might have exceeded the maximum size of 1048kb. Please choose a photo with a smaller size. If this is not the reason, please contact us at ###";
        var action = "Close";
        this._snackBar.open(message, action);
      });

  }

  getCustomerDecompressedImage() {
    this.store.dispatch(new ProfileActions.GetById(this.customer.id));
    this.store.select('profile')
    .pipe(take(1))
    .subscribe(profileState => {
      if(profileState.getByIdSuccess){
        let newJwtResponse: JwtResponse = new JwtResponse(
        profileState.customerById,
        this.authService.getToken(),
        this.authService.getRoles()
        );

        this.authService.storeUpdatedCustomerData(newJwtResponse)
      }
    });
  }

  getImgContent() {
    if(this.customer.imageCustomer != null)
      return this.sanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + this.customer.imageCustomer.picByte.toString());
    else
       return '../assets/images/avatar.png';
  }

  async onUpdate(updateForm: { value: Customer; reset: () => void; }) {
    this.changePassword$.next(false);
    this.update$.next(true);

    if(this.updateForm.valid){
      this.store.dispatch(new ProfileActions.Update(updateForm.value))

      this.storeUpdateSub = this.store.select('profile').subscribe(profileState => {

        this.changePassword$.pipe(take(1)).subscribe(changePassword => {

          this.update$.pipe(take(1)).subscribe(update => {

            if(profileState.updateSuccess && !changePassword && update){
              this.customer = profileState.customer;
              this.dialog.open(MessageDialogComponent, {
                  width: '500px',
                  data: {message: '<div>Account successfully updated!</div>'}
              });
              
             this.update$.next(false);
            }

          })
         
        })
      })
    }

  }

  onAskDelete(updateForm: { value: Customer; reset: () => void; }){
    this.dialog.open(DeleteDialogComponent, {
      width: '500px',
      data: {updateForm: updateForm}
    });
  }

  ngOnDestroy(): void {
    this.storeUpdateSub?.unsubscribe();
    this.storeGetByIdSub?.unsubscribe();
    this.storeChangePasswordSub?.unsubscribe();
  }

}


