import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import * as fromApp from '../../../store/app.reducer';
import * as CartActions from '../store/cart.actions';
import { Store } from '@ngrx/store';
import { CartService } from 'src/app/services/cart.service';
import { Product } from 'src/app/models/product.model';
import { Subscription } from 'rxjs';
import { Cart } from 'src/app/models/cart.model';

@Component({
  selector: 'app-cart-item-counter',
  templateUrl: './cart-item-counter.component.html',
  styleUrls: ['./cart-item-counter.component.less']
})
export class CartItemCounterComponent implements OnInit, OnDestroy {
  @Input() qty: number = 1;
  @Input() product: Product;
  @Output() displayStockError = new EventEmitter<boolean>();
  @Output() cart = new EventEmitter<Cart>();
  storeUpdateDataSub: Subscription;

  constructor(
    private store: Store<fromApp.AppState>,
    private cartService: CartService
  ) { }

  ngOnInit(): void {
  }

  onDecrementQty() {
    this.qty = this.qty - 1 > 0 ? this.qty-=1 : this.qty;
    this.updateCart();
  }

  onIncrementQty() {
    if(this.qty+1 <= this.product.quantity) {
      this.qty++;
      this.displayStockError.emit(false);
      this.updateCart();
    } else {
      this.displayStockError.emit(true);
    }
  }

  onChangeQty($event) {
    this.qty = $event.target.value;

    if(this.qty != null && this.qty > 0) {
      this.updateCart();
    }
  }

  updateCart() {
    this.store.dispatch(new CartActions.UpdateData(this.product, this.cartService.getCartId(), this.qty));
    this.storeUpdateDataSub = this.store.select('cart')
      .subscribe(cartState => {
        if(cartState.updateDataSuccess){
          this.cartService.setCart(cartState.cartResponse);
          this.cart.emit(cartState.cartResponse);
        }
    });
  }

  ngOnDestroy(): void {
    this.storeUpdateDataSub?.unsubscribe();
  }

}
