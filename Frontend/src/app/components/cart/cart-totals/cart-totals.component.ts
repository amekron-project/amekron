import { Component, Input, OnInit } from '@angular/core';
import { Cart } from 'src/app/models/cart.model';

@Component({
  selector: 'app-cart-totals',
  templateUrl: './cart-totals.component.html',
  styleUrls: ['./cart-totals.component.less']
})
export class CartTotalsComponent implements OnInit {

  @Input() cart: Cart;

  constructor() { }

  ngOnInit(): void {
  }

  onApplyCoupon() {

  }

}
