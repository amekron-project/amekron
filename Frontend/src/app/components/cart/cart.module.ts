import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartItemsComponent } from './cart-items/cart-items.component';
import { FooterSharedModule } from '../footer-shared-module/footer-shared-module.module';
import { CartRoutingModule } from './cart-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CartComponent } from './cart.component';
import { CartItemCounterComponent } from './cart-item-counter/cart-item-counter.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { CartTotalsComponent } from './cart-totals/cart-totals.component';
import {MatStepperModule} from '@angular/material/stepper';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { PersonalInfoFormComponent } from './checkout/personal-info-form/personal-info-form.component';
import { DeliveryAddressFormComponent } from './checkout/delivery-address-form/delivery-address-form.component';
import { PaymentDetailsFormComponent } from './checkout/payment-details-form/payment-details-form.component';
import { FinalFormComponent } from './checkout/final-form/final-form.component';
import { OrderPlacedComponent } from './order-placed/order-placed.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';


@NgModule({
  declarations: [
    CartComponent,
    CartItemsComponent,
    CartItemCounterComponent,
    CheckoutComponent,
    CartTotalsComponent,
    PersonalInfoFormComponent,
    DeliveryAddressFormComponent,
    PaymentDetailsFormComponent,
    FinalFormComponent,
    OrderPlacedComponent
  ],
  imports: [
    CommonModule,
    CartRoutingModule,
    FooterSharedModule,
    MatButtonModule,
    ReactiveFormsModule,
    FormsModule,
    MatCheckboxModule,
    MatButtonModule,
    MatStepperModule,
    MatIconModule,
    MatProgressSpinnerModule
  ]
})
export class CartModule { }
