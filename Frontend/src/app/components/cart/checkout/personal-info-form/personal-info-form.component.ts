import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { Customer } from 'src/app/models/customer.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-personal-info-form',
  templateUrl: './personal-info-form.component.html',
  styleUrls: ['./personal-info-form.component.less']
})
export class PersonalInfoFormComponent implements OnInit {

  @Input() personalInfoForm: FormGroup = new FormGroup({});
  customer: Customer;
  
  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.customer = this.authService.getSignedinUser();
  }

  onValidateForm() {
    this.personalInfoForm.markAllAsTouched();
  }

  onCompleteWithData(checkbox: MatCheckboxChange) {
    if (checkbox.checked)
    {
      this.personalInfoForm.get('firstName').setValue(this.customer.firstName);
      this.personalInfoForm.get('lastName').setValue(this.customer.lastName);
      this.personalInfoForm.get('email').setValue(this.customer.email);
      this.personalInfoForm.get('phone').setValue(this.customer.phone);
    }
    else {
      this.personalInfoForm.reset();
    }
 }

}
