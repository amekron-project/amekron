import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { take } from 'rxjs';
import { Cart } from 'src/app/models/cart.model';
import { CartService } from 'src/app/services/cart.service';
import * as fromApp from '../../../store/app.reducer';
import * as CartActions from '../store/cart.actions';
import { Store } from '@ngrx/store';
import { SpinnerService } from 'src/app/services/spinner.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.less']
})
export class CheckoutComponent implements OnInit {
  cart: Cart;
  finalForm: FormGroup = new FormGroup({});
  paymentDetailsForm: FormGroup = new FormGroup({});
  showSpinner = false;

  constructor(
    private cartService: CartService,
    private spinnerService: SpinnerService,
    private store: Store<fromApp.AppState>,
    private fb: FormBuilder,) { }

  ngOnInit(): void {
    this.spinnerService.showSpinner.subscribe(res => this.showSpinner = res);

    if(this.cartService.getCartId() != null) {
      this.store.dispatch(new CartActions.FindCartById(this.cartService.getCartId()));
      this.store.select('cart')
      .pipe(take(1))
      .subscribe(cartState => {
          if(cartState.findByIdSuccess){
            this.cart = cartState.cartResponse;
          }
      });
    }

    this.finalForm = this.fb.group({
      personalInfoForm: this.fb.group({
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        email: ['', Validators.required],
        phone: ['', Validators.required]
      }),

      deliveryAddressForm: this.fb.group({
        address: ['', Validators.required],
        city: ['', Validators.required],
        country: ['', Validators.required],
        zipCode: ['']
      })
      
    });

    
    this.paymentDetailsForm = this.fb.group({
      cardName: ['', Validators.required],
      cardNumber: ['', Validators.required],
      expiryDate: ['', Validators.required],
      securityCode: ['', Validators.required]
    })
  }

  get personalInfoForm() {
    return this.finalForm.controls['personalInfoForm'] as FormGroup;
  }

  get deliveryAddressForm() {
    return this.finalForm.controls['deliveryAddressForm'] as FormGroup;
  }

}
