import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { Cart } from 'src/app/models/cart.model';
import { Customer } from 'src/app/models/customer.model';
import { Order } from 'src/app/models/order.model';
import { AuthService } from 'src/app/services/auth.service';
import { CartService } from 'src/app/services/cart.service';
import { SpinnerService } from 'src/app/services/spinner.service';
import * as fromApp from '../../../../store/app.reducer';
import * as CartActions from '../../store/cart.actions';

@Component({
  selector: 'app-final-form',
  templateUrl: './final-form.component.html',
  styleUrls: ['./final-form.component.less']
})
export class FinalFormComponent implements OnInit, OnDestroy {

  order: Order;
  customer: Customer;
  @Input() finalForm: FormGroup = new FormGroup({});
  @Input() cart: Cart;
  storeSub: Subscription;
  showSpinner = false;
  
  constructor(
    private store: Store<fromApp.AppState>, 
    private authService: AuthService,
    private cartService: CartService,
    private spinnerService: SpinnerService,
    private router: Router) { }

  ngOnInit(): void {
    this.spinnerService.showSpinner.subscribe(res => this.showSpinner = res);
  }

  get personalInfoForm() {
    return this.finalForm.controls['personalInfoForm'] as FormGroup;
  }

  get deliveryAddressForm() {
    return this.finalForm.controls['deliveryAddressForm'] as FormGroup;
  }

  onOrder() {
    this.customer = this.authService.getSignedinUser();

    if(this.customer == null) {
      this.customer = new Customer(
        null,
        this.personalInfoForm.get('firstName').value,
        this.personalInfoForm.get('lastName').value,
        this.personalInfoForm.get('email').value,
        'empty',
        this.deliveryAddressForm.get('address').value,
        this.deliveryAddressForm.get('country').value,
        this.deliveryAddressForm.get('city').value,
        this.deliveryAddressForm.get('zipCode').value,
        this.personalInfoForm.get('phone').value,
        null
      );

    }

    this.order = new Order(null, this.cart, this.customer);

    this.store.dispatch(new CartActions.AddOrder(this.order));
    this.storeSub = this.store.select('cart')
      .subscribe(cartState => {
        if(cartState.addOrderSuccess && !this.showSpinner){
          this.cartService.clearCart();
          this.router.navigate(['cart/order-placed']);
        } 
    }); 
  }

  ngOnDestroy(): void {
    this.storeSub?.unsubscribe();
  }

}
