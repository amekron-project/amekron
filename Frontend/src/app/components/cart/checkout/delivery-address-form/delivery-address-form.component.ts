import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { Customer } from 'src/app/models/customer.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-delivery-address-form',
  templateUrl: './delivery-address-form.component.html',
  styleUrls: ['./delivery-address-form.component.less']
})
export class DeliveryAddressFormComponent implements OnInit {

  @Input() deliveryAddressForm: FormGroup = new FormGroup({});
  customer: Customer;
  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.customer = this.authService.getSignedinUser();
  }

  onValidateForm() {
    this.deliveryAddressForm.markAllAsTouched();
  }

  onCompleteWithData(checkbox: MatCheckboxChange) {
    if (checkbox.checked)
    {
      this.deliveryAddressForm.get('address').setValue(this.customer.address);
      this.deliveryAddressForm.get('city').setValue(this.customer.city);
      this.deliveryAddressForm.get('country').setValue(this.customer.country);
      this.deliveryAddressForm.get('zipCode').setValue(this.customer.zipCode);
    }
    else {
      this.deliveryAddressForm.reset();
    }
 }

}
