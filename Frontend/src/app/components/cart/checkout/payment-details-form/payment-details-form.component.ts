import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-payment-details-form',
  templateUrl: './payment-details-form.component.html',
  styleUrls: ['./payment-details-form.component.less']
})
export class PaymentDetailsFormComponent implements OnInit {

  @Input() paymentDetailsForm: FormGroup = new FormGroup({});
  
  constructor() { }

  ngOnInit(): void {
  }

  onValidateForm() {
    this.paymentDetailsForm.markAllAsTouched();
  }

}
