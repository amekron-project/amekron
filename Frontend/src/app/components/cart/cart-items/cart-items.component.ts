import { Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Cart } from 'src/app/models/cart.model';
import { Product } from 'src/app/models/product.model';
import { CartService } from 'src/app/services/cart.service';
import { ProductService } from 'src/app/services/product.service';
import * as fromApp from '../../../store/app.reducer';
import * as CartActions from '../store/cart.actions';
import { Subscription, take } from 'rxjs';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-cart-items',
  templateUrl: './cart-items.component.html',
  styleUrls: ['./cart-items.component.less']
})
export class CartItemsComponent implements OnDestroy {
  @ViewChild('stockErrorAlert', { static: false }) stockErrorAlert: ElementRef;
  @Input() cart: Cart;
  stockError: boolean = false;
  storeDeleteDataSub: Subscription;

  constructor(
    private store: Store<fromApp.AppState>,
    private router: Router,
    private cartService: CartService,
    private productService: ProductService) { }

  closeAlert() {
    this.stockErrorAlert.nativeElement.classList.remove('show');
  }

  onSelectProduct(product: Product) {
    this.productService.setSelectedProduct(product);
    this.router.navigate(['../catalogue/product_details', `${product.name}`]);
  }

  getImgContent(imgFile: string) {
    return this.productService.getImgContent(imgFile);
  }

  stockErrorChange($event: any) {
    this.stockError = $event;
  }

  cartChange($event: any) {
    this.cart = $event;
  }

  onRemoveProduct(productId: number) {
    this.store.dispatch(new CartActions.DeleteData(this.cart.id, productId));
    this.storeDeleteDataSub = this.store.select('cart')
      .subscribe(cartState => {
        if(cartState.deleteDataSuccess){
          this.cartService.setCart(cartState.cartResponse);
          this.cart = cartState.cartResponse;
        } 
    });
  }

  onContinueShopping() {
    this.router.navigate(['/catalogue']);
  }

  onNavigateToCheckout() {
    this.cartService.onCheckout.next(true);
    this.router.navigate(['cart/checkout']);
  }

  ngOnDestroy(): void {
    this.storeDeleteDataSub?.unsubscribe();
  }
}
