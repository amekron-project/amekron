import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription, take } from 'rxjs';
import { Cart } from '../../models/cart.model';
import { CartService } from '../../services/cart.service';
import * as fromApp from '../../store/app.reducer';
import * as CartActions from './store/cart.actions';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.less']
})
export class CartComponent implements OnInit, OnDestroy {
  cart: Cart;
  onCheckout: boolean = false;
  cartFindByIdSub: Subscription;

  constructor(
    private cartService: CartService,
    private store: Store<fromApp.AppState>,) { }

  ngOnInit(): void {
  
    if(this.cartService.getCartId() != null) {
      this.store.dispatch(new CartActions.FindCartById(this.cartService.getCartId()));
      this.cartFindByIdSub = this.store.select('cart')
      .subscribe(cartState => {
          if(cartState.findByIdSuccess){
            this.cart = cartState.cartResponse;
          }
      });
    }

    this.cartService.onCheckout.pipe(take(1)).subscribe(res => {
      this.onCheckout = res;
    })

  }

  ngOnDestroy(): void {
    this.cartFindByIdSub?.unsubscribe();
  }

}
