import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Store } from "@ngrx/store";
import { catchError, map, of, switchMap, withLatestFrom } from "rxjs";
import * as fromApp from '../../../store/app.reducer';
import * as CartActions from './cart.actions';
import { environment } from "src/environments/environment";
import { Cart } from "src/app/models/cart.model";
import { SpinnerService } from "src/app/services/spinner.service";

@Injectable()
export class CartEffects {
    constructor(
        private actions$: Actions,
        private http: HttpClient,
        private spinnerService: SpinnerService,
        private store: Store<fromApp.AppState>
    ){}

    @Effect()
    findCartById = this.actions$.pipe(
        ofType(CartActions.FIND_CART_BY_ID),
        withLatestFrom(this.store.select('cart')),
        switchMap(([actionData, cartState]) => {
            return this.http.get<Cart>(`${environment.apiBaseUrl}/cart/findById/${cartState.payloadCartId}`)
            .pipe(
                map(resData => { return new CartActions.FindCartByIdSuccess(resData);}),
                catchError(errorRes => {
                    return of(new CartActions.FindCartByIdFail())
                })
            )
        }) 
    )

    @Effect()
    addOrder = this.actions$.pipe(
        ofType(CartActions.ADD_ORDER),
        withLatestFrom(this.store.select('cart')),
        switchMap(([actionData, cartState]) => {
            this.spinnerService.showSpinner.next(true);
            return this.http.post<Cart>(`${environment.apiBaseUrl}/order/add`, cartState.payloadOrder)
            .pipe(
                map(() => { 
                    this.spinnerService.showSpinner.next(false);
                    return new CartActions.AddOrderSuccess();
                }),
                catchError(() => {
                    return of(new CartActions.AddOrderFail())
                })
            )
        })
    )

    @Effect()
    cartAddData = this.actions$.pipe(
        ofType(CartActions.ADD_DATA),
        withLatestFrom(this.store.select('cart')),
        switchMap(([actionData, cartState]) => {
            return this.http.post<Cart>(`${environment.apiBaseUrl}/cart/add/${cartState.payloadCartId}`, cartState.payloadProduct)
            .pipe(
                map(resData => { return new CartActions.AddDataSuccess(resData);}),
                catchError(errorRes => {
                    return of(new CartActions.AddDataFail())
                })
            )
        })
    )

    @Effect()
    cartUpdateData = this.actions$.pipe(
        ofType(CartActions.UPDATE_DATA),
        withLatestFrom(this.store.select('cart')),
        switchMap(([actionData, cartState]) => {
            return this.http.put<Cart>(`${environment.apiBaseUrl}/cart/update/${cartState.payloadCartId}/${cartState.payloadQty}`, cartState.payloadProduct)
            .pipe(
                map(resData => { return new CartActions.UpdateDataSuccess(resData);}),
                catchError(errorRes => {
                    return of(new CartActions.UpdateDataFail())
                })
            )
        })
    )

    @Effect()
    cartDeleteData = this.actions$.pipe(
        ofType(CartActions.DELETE_DATA),
        withLatestFrom(this.store.select('cart')),
        switchMap(([actionData, cartState]) => {
            return this.http.delete<Cart>(`${environment.apiBaseUrl}/cart/delete/${cartState.payloadCartId}/${cartState.payloadProductId}`)
            .pipe(
                map(resData => { return new CartActions.DeleteDataSuccess(resData);}),
                catchError(errorRes => {
                    return of(new CartActions.DeleteDataFail())
                })
            )
        })
    )

}
