import { Action } from "@ngrx/store";
import { Cart } from "src/app/models/cart.model";
import { Order } from "src/app/models/order.model";
import { Product } from "src/app/models/product.model";

export const ADD_DATA = "[Cart] ADD_DATA";
export const ADD_DATA_SUCCESS = "[Cart] ADD_DATA_SUCCESS";
export const ADD_DATA_FAIL = "[Cart] ADD_DATA_FAIL";
export const UPDATE_DATA = "[Cart] UPDATE_DATA";
export const UPDATE_DATA_SUCCESS = "[Cart] UPDATE_DATA_SUCCESS";
export const UPDATE_DATA_FAIL = "[Cart] UPDATE_DATA_FAIL";
export const DELETE_DATA = "[Cart] DELETE_DATA";
export const DELETE_DATA_SUCCESS = "[Cart] DELETE_DATA_SUCCESS";
export const DELETE_DATA_FAIL = "[Cart] DELETE_DATA_FAIL";
export const ADD_ORDER = "[Cart] ADD_ORDER";
export const ADD_ORDER_SUCCESS = "[Cart] ADD_ORDER_SUCCESS";
export const ADD_ORDER_FAIL = "[Cart] ADD_ORDER_FAIL";
export const FIND_CART_BY_ID = "[Cart] FIND_CART_BY_ID";
export const FIND_CART_BY_ID_SUCCESS = "[Cart] FIND_CART_BY_ID_SUCCESS";
export const FIND_CART_BY_ID_FAIL = "[Cart] FIND_CART_BY_ID_FAIL";

export class FindCartById implements Action {
  readonly type = FIND_CART_BY_ID;

  constructor(public payload: number){}
}

export class FindCartByIdSuccess implements Action {
  readonly type = FIND_CART_BY_ID_SUCCESS;

  constructor(public payload: Cart) {}
}

export class FindCartByIdFail implements Action {
  readonly type = FIND_CART_BY_ID_FAIL;
}

export class AddData implements Action {
  readonly type = ADD_DATA;

  constructor(public payloadProduct: Product, public payloadCartId: number){}
}

export class AddDataFail implements Action {
  readonly type = ADD_DATA_FAIL;
}

export class AddDataSuccess implements Action {
  readonly type = ADD_DATA_SUCCESS;

  constructor(public payload: Cart){}
}

export class UpdateData implements Action {
  readonly type = UPDATE_DATA;

  constructor(public payloadProduct: Product, public payloadCartId: number, public payloadQty: number){}
}

export class UpdateDataFail implements Action {
  readonly type = UPDATE_DATA_FAIL;
}

export class UpdateDataSuccess implements Action {
  readonly type = UPDATE_DATA_SUCCESS;

  constructor(public payload: Cart){}
}

export class DeleteData implements Action {
  readonly type = DELETE_DATA;

  constructor(public payloadCartId: number, public payloadProductId: number) {}
}

export class DeleteDataSuccess implements Action {
  readonly type = DELETE_DATA_SUCCESS;

  constructor(public payload: Cart){}
}

export class DeleteDataFail implements Action {
  readonly type = DELETE_DATA_FAIL;
}

export class AddOrder implements Action {
  readonly type = ADD_ORDER;

  constructor(public payload: Order){}
}

export class AddOrderSuccess implements Action {
  readonly type = ADD_ORDER_SUCCESS;
}

export class AddOrderFail implements Action {
  readonly type = ADD_ORDER_FAIL;
}

  export type CartActions =
| AddData
| AddDataFail
| AddDataSuccess
| UpdateData
| UpdateDataFail
| UpdateDataSuccess
| DeleteData
| DeleteDataFail
| DeleteDataSuccess
| AddOrder
| AddOrderFail
| AddOrderSuccess
| FindCartById
| FindCartByIdSuccess
| FindCartByIdFail;