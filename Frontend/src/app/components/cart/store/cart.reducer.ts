import { Cart } from 'src/app/models/cart.model';
import { Order } from 'src/app/models/order.model';
import { Product } from 'src/app/models/product.model';
import * as CartActions from './cart.actions';

export interface State {
    findByIdSuccess: boolean;
    payloadProduct: Product;
    payloadCartId: number;
    payloadProductId: number;
    payloadQty: number;
    addDataSuccess: boolean;
    updateDataSuccess: boolean;
    deleteDataSuccess: boolean;
    cartResponse: Cart;
    payloadOrder: Order;
    addOrderSuccess: boolean;
  }
  
  const initialState: State = {
    findByIdSuccess: false,
    payloadProduct: null,
    payloadCartId: null,
    payloadProductId: null,
    payloadQty: null,
    addDataSuccess: false,
    updateDataSuccess: false,
    deleteDataSuccess: false,
    cartResponse: null,
    payloadOrder: null,
    addOrderSuccess: false
  }
  
  export function cartReducer(state = initialState, action: CartActions.CartActions){
    switch (action.type){
        case CartActions.FIND_CART_BY_ID:
          const payloadCartIdFind = action.payload;

          return {
            ...state,
            payloadCartId: payloadCartIdFind
          }

        case CartActions.FIND_CART_BY_ID_SUCCESS:
          const payloadCartFind = action.payload;
          
          return {
            ...state,
            cartResponse: payloadCartFind,
            findByIdSuccess: true
          }

        case CartActions.FIND_CART_BY_ID_FAIL:
          return {
            ...state,
            findByIdSuccess: false
          }

        case CartActions.ADD_DATA:
          const payloadProductAdd = action.payloadProduct;
          const payloadCartIdAdd = action.payloadCartId;

          return {
            ...state,
            payloadProduct: payloadProductAdd,
            payloadCartId: payloadCartIdAdd
          }

        case CartActions.ADD_DATA_SUCCESS:
          const cartResponseAdd = action.payload;

          return {
            ...state,
            cartResponse: cartResponseAdd,
            addDataSuccess: true
          }

        case CartActions.ADD_DATA_FAIL:
          return {
            ...state,
           addDataSuccess: false
          }

        case CartActions.UPDATE_DATA:
          const payloadProductUpdate = action.payloadProduct;
          const payloadCartIdUpdate = action.payloadCartId;
          const payloadQtyUpdate = action.payloadQty;
  
            return {
              ...state,
              payloadProduct: payloadProductUpdate,
              payloadCartId: payloadCartIdUpdate,
              payloadQty: payloadQtyUpdate
            }
  
        case CartActions.UPDATE_DATA_SUCCESS:
          const cartResponseUpdate = action.payload;
  
          return {
            ...state,
            cartResponse: cartResponseUpdate,
            updateDataSuccess: true
          }
  
        case CartActions.UPDATE_DATA_FAIL:
          return {
            ...state,
            updateDataSuccess: false
          }
  
        case CartActions.DELETE_DATA:
          const payloadProductIdDelete = action.payloadProductId;
          const payloadCartIdDelete = action.payloadCartId;
    
          return {
            ...state,
            payloadProductId: payloadProductIdDelete,
            payloadCartId: payloadCartIdDelete
          }
    
        case CartActions.DELETE_DATA_SUCCESS:
          const cartResponseDelete = action.payload;
    
          return {
            ...state,
            cartResponse: cartResponseDelete,
            deleteDataSuccess: true
          }
    
        case CartActions.UPDATE_DATA_FAIL:
          return {
            ...state,
            deleteDataSuccess: false
        }

        case CartActions.ADD_ORDER:
          const payloadOrder = action.payload;

          return {
            ...state,
            payloadOrder: payloadOrder
          }

        case CartActions.ADD_ORDER_SUCCESS:
          return {
            ...state,
            addOrderSuccess: true
          }


        case CartActions.ADD_ORDER_FAIL:
          return {
            ...state,
            addOrderSuccess: false
          }
        
        default:
            return state;
    }
  }
  