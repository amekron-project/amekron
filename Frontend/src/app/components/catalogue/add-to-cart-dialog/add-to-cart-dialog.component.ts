import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Cart } from 'src/app/models/cart.model';
import { CartService } from 'src/app/services/cart.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-add-to-cart-dialog',
  templateUrl: './add-to-cart-dialog.component.html',
  styleUrls: ['./add-to-cart-dialog.component.less']
})
export class AddToCartDialogComponent{

  constructor(
    private productService: ProductService,
    public dialogRef: MatDialogRef<AddToCartDialogComponent>,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  onContinueShopping(): void {
    this.dialogRef.close();
  }

  onNavigateToCart() {
    this.router.navigate(['/cart']);

    this.onContinueShopping();
  }

  getImgContent(imgFile: string) {
    return this.productService.getImgContent(imgFile);
  }

}
