import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Product } from 'src/app/models/product.model';
import { AddToCartDialogComponent } from '../add-to-cart-dialog/add-to-cart-dialog.component';
import * as fromApp from '../../../store/app.reducer';
import * as CartActions from '../../cart/store/cart.actions';
import { Store } from '@ngrx/store';
import { Subscription, take } from 'rxjs';
import { CartService } from 'src/app/services/cart.service';
import { Cart } from 'src/app/models/cart.model';

@Component({
  selector: 'app-product-item-details',
  templateUrl: './product-item-details.component.html',
  styleUrls: ['./product-item-details.component.less']
})
export class ProductItemDetailsComponent implements OnInit, OnDestroy {
  @Input() product: Product;
  @Input() productRating: number;
  storeAddDataSub: Subscription;
  cart: Cart;
  
  constructor(
    private dialog: MatDialog,
    private store: Store<fromApp.AppState>,
    private cartService: CartService) { }

  ngOnInit(): void {
    this.productRating = this.productRating ? this.productRating : this.product.rating;
  }

  addToCart() {
    this.getByCartId();

    let product =  this.cart?.products?.find(p => p.id == this.product.id);

    if(product == null) {
        this.storeAddDataSub = this.store.select('cart')
        .subscribe(cartState => {
          if(cartState.addDataSuccess){
            this.cartService.setCart(cartState.cartResponse);
          }
        });
    }

    this.dialog.open(AddToCartDialogComponent, {
      width: '500px',
      data: {
        product: this.product,
        inCart: product
      }
    });
  }

  getByCartId() {
    if(this.cartService.getCartId() != null) {
      this.store.dispatch(new CartActions.FindCartById(this.cartService.getCartId()));
      this.store.select('cart')
      .pipe(take(1))
      .subscribe(cartState => {
          if(cartState.findByIdSuccess){
            this.cart = cartState.cartResponse;
            this.store.dispatch(new CartActions.AddData(this.product, this.cart.id));
          }
      });
    } else {
      this.store.dispatch(new CartActions.AddData(this.product, -1));
    }
  }

  ngOnDestroy(): void {
    this.storeAddDataSub?.unsubscribe();
  }

}
