import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/models/product.model';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.less']
})
export class ProductItemComponent implements OnInit {
  @Input() product: Product;

  constructor(private productService: ProductService, private router: Router) { }

  ngOnInit(): void {
  }

  onSelectProduct() {
    this.productService.setSelectedProduct(this.product);
    this.router.navigate(['../catalogue/product_details', `${this.product.name}`]);
  }

  getImgContent(imgFile: string) {
    return this.productService.getImgContent(imgFile);
  }

}
