import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogueRoutingModule } from './catalogue-routing.module';
import { PdpComponent } from './pdp/pdp.component';
import { PlpComponent } from './plp/plp.component';
import { ProductItemComponent } from './product-item/product-item.component';
import { ProductItemDetailsComponent } from './product-item-details/product-item-details.component';
import { ProductSearchComponent } from './product-search/product-search.component';
import { FooterSharedModule } from '../footer-shared-module/footer-shared-module.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatTabsModule } from '@angular/material/tabs';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { AddToCartDialogComponent } from './add-to-cart-dialog/add-to-cart-dialog.component';


@NgModule({
  declarations: [
    PdpComponent,
    PlpComponent,
    ProductItemComponent,
    ProductItemDetailsComponent,
    ProductSearchComponent,
    AddToCartDialogComponent
  ],
  imports: [
    CommonModule,
    CatalogueRoutingModule,
    FooterSharedModule,
    MatIconModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    NgbModule,
    FormsModule,
    MatExpansionModule,
    MatCheckboxModule,
    MatRadioModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatTabsModule,
    MatInputModule
  ]
})
export class CatalogueModule { }
