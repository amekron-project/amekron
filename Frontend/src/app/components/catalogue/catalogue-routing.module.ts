import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PdpComponent } from './pdp/pdp.component';
import { PlpComponent } from './plp/plp.component';
import { ProductSearchComponent } from './product-search/product-search.component';

const routes: Routes = [
  {
    path: '', component: PlpComponent
  },
  {
    path: 'product_details/:name', component: PdpComponent
  },
  {
    path: 'results', 
    component: ProductSearchComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogueRoutingModule { }
