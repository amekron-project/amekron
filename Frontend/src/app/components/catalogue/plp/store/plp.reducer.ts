import { Product } from 'src/app/models/product.model';
import { Promotion } from 'src/app/models/promotion.model';
import * as PlpActions from './plp.actions';

export interface State {
  products: Product[];
  selectedCategories: string[];
  selectedPromotions: string[];
  productsByCategoriesAndPromotions: Product[];
  promotionsProductDisplay: string[];
  categories: string[];
  promotions: Promotion[];
  findAllSuccess: boolean;
  getAllSuccess: boolean;
  findByCategoriesAndPromotionsSuccess: boolean;
}

const initialState: State = {
  products: null,
  selectedCategories: null,
  selectedPromotions: null,
  productsByCategoriesAndPromotions: null,
  promotionsProductDisplay: null,
  categories: null,
  promotions: null,
  findAllSuccess: false,
  getAllSuccess: false,
  findByCategoriesAndPromotionsSuccess: false
}

export function plpReducer(state = initialState, action: PlpActions.PlpActions){
  switch (action.type){
      case PlpActions.FIND_ALL:
          return {
              ...state
          }

      case PlpActions.FIND_ALL_SUCCESS:
        const products = action.payload;
          return {
              ...state,
              products: products,
              findAllSuccess: true
          }

      case PlpActions.FIND_ALL_FAIL:
          return {
              ...state,
              findAllSuccess: false
          }


      case PlpActions.FIND_BY_CATEGORIES_AND_PROMOTIONS:
          const productsPromotions = action.payloadPromotions;
          const productsCategories = action.payloadCategories;
          return {
                ...state,
                selectedCategories: productsCategories,
                selectedPromotions: productsPromotions
          }

      case PlpActions.FIND_BY_CATEGORIES_AND_PROMOTIONS_SUCCESS:
          const productsByCategoriesAndPromotions = action.payload;
          return {
                ...state,
                productsByCategoriesAndPromotions: productsByCategoriesAndPromotions,
                findByCategoriesAndPromotionsSuccess: true
          }


      case PlpActions.FIND_BY_CATEGORIES_AND_PROMOTIONS_FAIL:
          return {
                ...state,
                findByCategoriesAndPromotionsSuccess: false
          }

      case PlpActions.GET_ALL:
          return {
              ...state
          }

      case PlpActions.GET_ALL_SUCCESS:
          const promotions = action.payload;
          return {
                ...state,
                promotions: promotions,
                getAllSuccess: true
          }


      case PlpActions.GET_ALL_FAIL:
          return {
                ...state,
                getAllSuccess: false
          }


      default:
          return state;
  }
}
