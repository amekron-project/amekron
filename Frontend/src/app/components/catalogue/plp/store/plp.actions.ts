import { Action } from "@ngrx/store";
import { Product } from "src/app/models/product.model";
import { Promotion } from "src/app/models/promotion.model";

export const FIND_ALL = "[Plp] FIND_ALL";
export const FIND_ALL_SUCCESS = "[Plp] FIND_ALL_SUCCESS";
export const FIND_ALL_FAIL = "[Plp] FIND_ALL_FAIL";
export const GET_ALL = "[Plp] GET_ALL";
export const GET_ALL_SUCCESS = "[Plp] GET_ALL_SUCCESS";
export const GET_ALL_FAIL = "[Plp] GET_ALL_FAIL";
export const FIND_BY_CATEGORIES_AND_PROMOTIONS = "[Plp] FIND_BY_CATEGORIES_AND_PROMOTIONS";
export const FIND_BY_CATEGORIES_AND_PROMOTIONS_SUCCESS = "[Plp] FIND_BY_CATEGORIES_AND_PROMOTIONS_SUCCESS";
export const FIND_BY_CATEGORIES_AND_PROMOTIONS_FAIL = "[Plp] FIND_BY_CATEGORIES_AND_PROMOTIONS_FAIL";

//for products
export class FindAll implements Action {
  readonly type = FIND_ALL;
}

export class FindAllSuccess implements Action {
  readonly type = FIND_ALL_SUCCESS;

  constructor(public payload: Product[]){}
}

export class FindAllFail implements Action {
  readonly type = FIND_ALL_FAIL;
}

export class FindByCategoriesAndPromotions implements Action {
  readonly type = FIND_BY_CATEGORIES_AND_PROMOTIONS;

  constructor(public payloadCategories: string[], public payloadPromotions: string[]){}
}

export class FindByCategoriesAndPromotionsSuccess implements Action {
  readonly type = FIND_BY_CATEGORIES_AND_PROMOTIONS_SUCCESS;

  constructor(public payload: Product[]){}
}

export class FindByCategoriesAndPromotionsFail implements Action {
  readonly type = FIND_BY_CATEGORIES_AND_PROMOTIONS_FAIL;
}

//for promotions
export class GetAll implements Action {
  readonly type = GET_ALL;
}

export class GetAllSuccess implements Action {
  readonly type = GET_ALL_SUCCESS;

  constructor(public payload: Promotion[]){}
}

export class GetAllFail implements Action {
  readonly type = GET_ALL_FAIL;
}


export type PlpActions =
| FindAll
| FindAllSuccess
| FindAllFail
| GetAll
| GetAllSuccess
| GetAllFail
| FindByCategoriesAndPromotions
| FindByCategoriesAndPromotionsSuccess
| FindByCategoriesAndPromotionsFail;
