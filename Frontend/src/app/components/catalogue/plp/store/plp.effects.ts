import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Store } from "@ngrx/store";
import { catchError, map, of, switchMap, withLatestFrom } from "rxjs";
import { Product } from "src/app/models/product.model";
import * as fromApp from '../../../../store/app.reducer';
import * as PlpActions from './plp.actions';
import { environment } from "src/environments/environment";
import { Promotion } from "src/app/models/promotion.model";

@Injectable()
export class PlpEffects {
    constructor(
        private actions$: Actions,
        private http: HttpClient,
        private store: Store<fromApp.AppState>
    ){}


    @Effect()
    plpFindAll = this.actions$.pipe(
        ofType(PlpActions.FIND_ALL),
        withLatestFrom(this.store.select('plp')),
        switchMap(([actionData, plpState]) => {
            return this.http.get<Product[]>(`${environment.apiBaseUrl}/product/findAll`)
            .pipe(
                map(resData => {return new PlpActions.FindAllSuccess(resData);}),
                catchError(errorRes => {
                    return of(new PlpActions.FindAllFail())
                })
            )
        })
    )


    @Effect()
    plpGetAll = this.actions$.pipe(
        ofType(PlpActions.GET_ALL),
        withLatestFrom(this.store.select('plp')),
        switchMap(([actionData, plpState]) => {
            return this.http.get<Promotion[]>(`${environment.apiBaseUrl}/promotion/getAll`)
            .pipe(
                map(resData => {return new PlpActions.GetAllSuccess(resData);}),
                catchError(errorRes => {
                    return of(new PlpActions.GetAllFail())
                })
            )
        })
    )

    @Effect()
    plpFindByCategoriesAndPromotions = this.actions$.pipe(
        ofType(PlpActions.FIND_BY_CATEGORIES_AND_PROMOTIONS),
        withLatestFrom(this.store.select('plp')),
        switchMap(([actionData, plpState]) => {
            return this.http.post<Product[]>(`${environment.apiBaseUrl}/product/findByCategoriesAndPromotions`, {selectedCategories: plpState.selectedCategories, selectedPromotions: plpState.selectedPromotions})
            .pipe(
                map(resData => {return new PlpActions.FindByCategoriesAndPromotionsSuccess(resData);}),
                catchError(errorRes => {
                    return of(new PlpActions.FindByCategoriesAndPromotionsFail())
                })
            )
        })
    )

}
