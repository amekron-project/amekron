import { HttpErrorResponse } from '@angular/common/http';
import { Component, ElementRef, HostListener, QueryList, Renderer2, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatRadioChange } from '@angular/material/radio';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { Product } from 'src/app/models/product.model';
import { Promotion } from 'src/app/models/promotion.model';
import { ProductService } from 'src/app/services/product.service';
import * as fromApp from '../../../store/app.reducer';
import * as PlpActions from './store/plp.actions';

@Component({
  selector: 'app-plp',
  templateUrl: './plp.component.html',
  styleUrls: ['./plp.component.less']
})
export class PlpComponent {
  isLoading = true;
  promotions: Promotion[] = [];
  products: Product[] = [];
  storeProductsFindAllSub: Subscription;
  storeProductsFindByCategoriesSub: Subscription;
  storeProductsFindByPromotionsSub: Subscription;
  categories: FormGroup;
  categoriesStrings: string[] = [];
  promotionsFg: FormGroup;
  promotionsStrings: string[] = [];
  st: string;//sortType
  noProdWithSelectedPromotions = false;
  noProdAvailable = false;
  currentProductsToShow = [];
  productsNumber: number;
  @ViewChildren('paginator') paginator: QueryList<MatPaginator>;

  constructor(
    private store: Store<fromApp.AppState>,
    private fb: FormBuilder,
    private _snackBar: MatSnackBar,
    private productService: ProductService,
    private elem: ElementRef,
    private renderer: Renderer2,
    ) {
    this.categories = fb.group({
      All: false,
      Animals: false,
      Art: false,
      Christmas: false,
      Entertainment: false,
      Halloween: false,
      Mythology: false,
      Others: false
    });

    this.promotionsFg = fb.group({
      All: false
    });

  }

  ngOnInit() {
      //for promotions
      this.store.dispatch(new PlpActions.GetAll());
  
      this.getAllProducts();
  }

  ngAfterViewChecked(){
    this.productService.calculateHeights(this.elem, this.renderer);
  }

  @HostListener('window:resize') onResize() {
    let resizeTimer;
    clearTimeout(resizeTimer);

    resizeTimer = setTimeout(() => {
      this.productService.calculateHeights(this.elem, this.renderer);
    }, 250);
  }

  onPageChange($event) {
    this.currentProductsToShow = this.products.slice(
      $event.pageIndex * $event.pageSize,
      $event.pageIndex * $event.pageSize + $event.pageSize
    );
  }

  displayByCategoriesAndPromotions(){
    this.promotionsStrings = [];
    for (const field in this.promotionsFg.controls) { 
      if(this.promotionsFg.controls[field].value)
        this.promotionsStrings.push(field);
    }

    if(this.promotionsStrings.includes('All') && this.promotionsStrings.length == 1){
      for (const field in this.promotionsFg.controls) {
        if(field != 'All')
        this.promotionsStrings.push(field);
      }
    }

    this.categoriesStrings = [];
    for (const field in this.categories.controls) {
      if(this.categories.controls[field].value)
        this.categoriesStrings.push(field);
    }

    if((this.categoriesStrings.length <= 0 || this.categoriesStrings.includes("All")) && this.promotionsStrings.length <= 0) {
      this.getAllProducts();
    } else {
      this.store.dispatch(new PlpActions.FindByCategoriesAndPromotions(this.categoriesStrings, this.promotionsStrings));
      this.storeProductsFindByCategoriesSub = this.store.select('plp')
      .subscribe(plpState => {
        if(plpState.findByCategoriesAndPromotionsSuccess){
          this.products = [...plpState.productsByCategoriesAndPromotions];

          if(this.products.length <= 0){
              this.noProdWithSelectedPromotions = true;
              this.noProdAvailable = false;
          } else {
            this.noProdAvailable = false;
            this.noProdWithSelectedPromotions = false;
            this.paginator.first.pageIndex = 0;
            this.currentProductsToShow = this.products.slice(0,6);
            this.productsNumber = this.products.length;
            this.sort(null, this.st);
          }

          this.isLoading = false;
        }
      },
      (error: HttpErrorResponse) => {
          var message = "Ops! We could not retrieve any products. Please contact us at ###";
          var action = "Close";
          this._snackBar.open(message, action);
      })
    }
  }

  getAllProducts() {
    //for products
    this.store.dispatch(new PlpActions.FindAll());
    this.storeProductsFindAllSub = this.store.select('plp')
    .subscribe(plpState => {
      if(plpState.findAllSuccess){
        this.products = [...plpState.products];

        if(this.products.length <= 0){
            this.noProdAvailable = true;
            this.noProdWithSelectedPromotions = false;
        } else {
          if(plpState.getAllSuccess){
            this.promotions = plpState.promotions;
  
            for(let promo of this.promotions){
              this.promotionsFg.addControl(promo.name, new FormControl(''));
            }
          }

          if(this.products.length > 0) {
            this.productsNumber = this.products.length;
            this.currentProductsToShow = this.products.slice(0,6);

            if(this.paginator != undefined && this.paginator.first != undefined)
              this.paginator.first.pageIndex = 0;
            this.sort(null, this.st);
          }
        }
        this.isLoading = false;
      }
    },
    (error: HttpErrorResponse) => {
        var message = "Ops! We could not retrieve any products. Please contact us at ###";
        var action = "Close";
        this._snackBar.open(message, action);
    })
  }

  sort(event: MatRadioChange, st: string){
    let sortType = event ? event.value : st;
    this.st = sortType;
    switch(sortType) {
      case "newest": {
         this.products.sort((p2, p1) => new Date(p1.creationDate).getTime() - new Date(p2.creationDate).getTime());
         this.currentProductsToShow = this.products.slice(0,6);
         this.paginator.first.pageIndex = 0;
         break;
      }
      case "oldest": {
         this.products.sort((p1, p2) => new Date(p1.creationDate).getTime() - new Date(p2.creationDate).getTime());
         this.currentProductsToShow = this.products.slice(0,6);
         this.paginator.first.pageIndex = 0;
         break;
      }
      case "ascending": {
        this.products.sort(function(p1, p2) {
          return p1.price.valueOf() - p2.price.valueOf();
        });
        this.currentProductsToShow = this.products.slice(0,6);
        this.paginator.first.pageIndex = 0;
        break;
      }
      case "descending": {
        this.products.sort(function(p2, p1) {
          return p1.price.valueOf() - p2.price.valueOf();
        });
        this.currentProductsToShow = this.products.slice(0,6);
        this.paginator.first.pageIndex = 0;
        break;
      }
    }
  }

}
