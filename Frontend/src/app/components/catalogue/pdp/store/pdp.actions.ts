import { Action } from "@ngrx/store";
import { Product } from "src/app/models/product.model";
import { Review } from "src/app/models/review.model";

export const FIND_BY_PROMOTIONS = "[Pdp] FIND_BY_PROMOTIONS";
export const FIND_BY_PROMOTIONS_SUCCESS = "[Pdp] FIND_BY_PROMOTIONS_SUCCESS";
export const FIND_BY_PROMOTIONS_FAIL = "[Pdp] FIND_BY_PROMOTIONS_FAIL";

export const ADD_REVIEW = "[Pdp] ADD_REVIEW";
export const ADD_REVIEW_FAIL = "[Pdp] ADD_REVIEW_FAIL";
export const GET_REVIEWS_BY_PRODUCT_ID = "[Pdp] GET_REVIEWS_BY_PRODUCT_ID";
export const GET_REVIEWS_BY_PRODUCT_ID_SUCCESS = "[Pdp] GET_REVIEWS_BY_PRODUCT_ID_SUCCESS";
export const GET_REVIEWS_BY_PRODUCT_ID_FAIL = "[Pdp] GET_REVIEWS_BY_PRODUCT_ID_FAIL";

export const DELETE_REVIEW = "[Pdp] DELETE_REVIEW";
export const DELETE_REVIEW_SUCCESS = "[Pdp] DELETE_REVIEW_SUCCESS";
export const DELETE_REVIEW_FAIL = "[Pdp] DELETE_REVIEW_FAIL";

export class AddReview implements Action {
  readonly type = ADD_REVIEW;

  constructor(public payload: Review){}
}

export class AddReviewFail implements Action {
  readonly type = ADD_REVIEW_FAIL;
}

export class GetReviewsByProductId implements Action {
  readonly type = GET_REVIEWS_BY_PRODUCT_ID;

  constructor(public payload: number){}
}

export class GetReviewsByProductIdSuccess implements Action {
  readonly type = GET_REVIEWS_BY_PRODUCT_ID_SUCCESS;

  constructor(public payload: Review[]){}
}

export class GetReviewsByProductIdFail implements Action {
  readonly type = GET_REVIEWS_BY_PRODUCT_ID_FAIL;
}

export class DeleteReview implements Action {
  readonly type = DELETE_REVIEW;

  constructor(public payload: number){}
}

export class DeleteReviewSuccess implements Action {
  readonly type = DELETE_REVIEW_SUCCESS;

  constructor(public payload: Review[]){}
}

export class DeleteReviewFail implements Action {
  readonly type = DELETE_REVIEW_FAIL;
}

//find by Mix and Match
export class FindByPromotions implements Action {
    readonly type = FIND_BY_PROMOTIONS;
  
    constructor(public payload: string[]){}
  }
  
  export class FindByPromotionsSuccess implements Action {
    readonly type = FIND_BY_PROMOTIONS_SUCCESS;
  
    constructor(public payload: Product[]){}
  }
  
  export class FindByPromotionsFail implements Action {
    readonly type = FIND_BY_PROMOTIONS_FAIL;
  }
  
  export type PdpActions =
| FindByPromotions
| FindByPromotionsSuccess
| FindByPromotionsFail
| AddReview
| AddReviewFail
| GetReviewsByProductId
| GetReviewsByProductIdFail
| GetReviewsByProductIdSuccess
| DeleteReview
| DeleteReviewSuccess
| DeleteReviewFail;