import { Product } from 'src/app/models/product.model';
import { Review } from 'src/app/models/review.model';
import * as PdpActions from './pdp.actions';

export interface State {
    carouselPromotions: string[];
    productsByPromotions: Product[];
    findByPromotionsSuccess: boolean;

    addReview: Review;
    productIdAddReview: number;
    getReviewsByProduct: Review[];
    addReviewSuccess: boolean;

    deleteReviewId: number;
    deleteReviewResponse: Review[];
    deleteReviewSuccess: boolean;
  }
  
  const initialState: State = {
    carouselPromotions: null,
    productsByPromotions: null,
    findByPromotionsSuccess: false,

    productIdAddReview: null,
    addReview: null,
    getReviewsByProduct: null,
    addReviewSuccess: false,

    deleteReviewId: null,
    deleteReviewResponse: null,
    deleteReviewSuccess: false
  }
  
  export function pdpReducer(state = initialState, action: PdpActions.PdpActions){
    switch (action.type){
        case PdpActions.DELETE_REVIEW:
          const deleteReviewId = action.payload;

          return {
            ...state,
            deleteReviewId: deleteReviewId
          }

        case PdpActions.DELETE_REVIEW_SUCCESS:
          const deleteReviewResponse = action.payload;

          return {
            ...state,
            deleteReviewResponse: deleteReviewResponse,
            deleteReviewSuccess: true
          }

        case PdpActions.DELETE_REVIEW_FAIL:
          return {
            ...state,
           deleteReviewSuccess: false
          }

        case PdpActions.ADD_REVIEW:
          const reviewToBeAdded = action.payload;
          return {
            ...state,
            addReview: reviewToBeAdded
          }

        case PdpActions.ADD_REVIEW_FAIL:
          return {
            ...state,
            addReviewSucces: false
          }

        case PdpActions.GET_REVIEWS_BY_PRODUCT_ID:
          const productId = action.payload;
          return {
            ...state,
            productIdAddReview: productId
          }

        case PdpActions.GET_REVIEWS_BY_PRODUCT_ID_SUCCESS:
          const reviewResponse = action.payload;
          return {
            ...state,
            getReviewsByProduct: reviewResponse,
            addReviewSuccess: true
          }

        case PdpActions.GET_REVIEWS_BY_PRODUCT_ID_FAIL:
          return {
            ...state,
            addReviewSuccess: false
          }

        case PdpActions.FIND_BY_PROMOTIONS:
            const carouselPromotions = action.payload;
            return {
                  ...state,
                  carouselPromotions: carouselPromotions
            }
  
        case PdpActions.FIND_BY_PROMOTIONS_SUCCESS:
            const productsByPromotions = action.payload;
            return {
                  ...state,
                  productsByPromotions: productsByPromotions,
                  findByPromotionsSuccess: true
            }
  
  
        case PdpActions.FIND_BY_PROMOTIONS_FAIL:
            return {
                  ...state,
                  findByPromotionsSuccess: false
            }
  
        default:
            return state;
    }
  }
  