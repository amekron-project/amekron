import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Store } from "@ngrx/store";
import { catchError, map, of, switchMap, withLatestFrom } from "rxjs";
import { Product } from "src/app/models/product.model";
import * as fromApp from '../../../../store/app.reducer';
import * as PdpActions from './pdp.actions';
import { environment } from "src/environments/environment";
import { Review } from "src/app/models/review.model";

@Injectable()
export class PdpEffects {
    constructor(
        private actions$: Actions,
        private http: HttpClient,
        private store: Store<fromApp.AppState>
    ){}


    @Effect()
    pdpFindByPromotions = this.actions$.pipe(
        ofType(PdpActions.FIND_BY_PROMOTIONS),
        withLatestFrom(this.store.select('pdp')),
        switchMap(([actionData, pdpState]) => {
            return this.http.post<Product[]>(`${environment.apiBaseUrl}/product/findByPromotions`, pdpState.carouselPromotions)
            .pipe(
                map(resData => { return new PdpActions.FindByPromotionsSuccess(resData);} ),
                catchError(errorRes => {
                    return of(new PdpActions.FindByPromotionsFail())
                })
            )
        })
    )

    @Effect()
    pdpGetReviewsByProductId = this.actions$.pipe(
        ofType(PdpActions.GET_REVIEWS_BY_PRODUCT_ID),
        withLatestFrom(this.store.select('pdp')),
        switchMap(([actionData, pdpState]) => {
            return this.http.get<Review[]>(`${environment.apiBaseUrl}/review/getByProductId/${pdpState.productIdAddReview}`)
            .pipe(
                map(resData => { return new PdpActions.GetReviewsByProductIdSuccess(resData);}),
                catchError(errorRes => {
                    return of(new PdpActions.GetReviewsByProductIdFail())
                })
            )
        })
    )

    @Effect()
    pdpAddReview = this.actions$.pipe(
        ofType(PdpActions.ADD_REVIEW),
        withLatestFrom(this.store.select('pdp')),
        switchMap(([actionData, pdpState]) => {
            return this.http.post<Review>(`${environment.apiBaseUrl}/review/add`, pdpState.addReview)
            .pipe(
                map(resData => { return new PdpActions.GetReviewsByProductId(pdpState.addReview.product.id);}),
                catchError(errorRes => {
                    return of(new PdpActions.AddReviewFail())
                })
            )
        })
    )

    @Effect()
    pdpDeleteReview = this.actions$.pipe(
        ofType(PdpActions.DELETE_REVIEW),
        withLatestFrom(this.store.select('pdp')),
        switchMap(([actionData, pdpState]) => {
            return this.http.delete<Review[]>(`${environment.apiBaseUrl}/review/delete/${pdpState.deleteReviewId}`)
            .pipe(
                map(resData => { return new PdpActions.DeleteReviewSuccess(resData);}),
                catchError(errorRes => {
                    return of(new PdpActions.DeleteReviewFail())
                })
            )
        })
    )

}
