import { Byte } from '@angular/compiler/src/util';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { Customer } from 'src/app/models/customer.model';
import { ImageCustomer } from 'src/app/models/imageCustomer.model';
import { Product } from 'src/app/models/product.model';
import { Review } from 'src/app/models/review.model';
import { AuthService } from 'src/app/services/auth.service';
import { ProductService } from 'src/app/services/product.service';
import * as fromApp from '../../../store/app.reducer';
import * as PdpActions from './store/pdp.actions';

@Component({
  selector: 'app-pdp',
  templateUrl: './pdp.component.html',
  styleUrls: ['./pdp.component.less']
})
export class PdpComponent implements OnInit, OnDestroy {
  carouselProducts: Product[];
  carouselProductsFormatted = [];
  product: Product;
  imagesFormatted=[];
  mainProductImage: SafeUrl;
  storeProductsFindByPromotionsSub: Subscription;
  storeAddEditReviewSub: Subscription;
  storeDeleteReviewSub: Subscription;
  storeGetReviewsByProductIdSub: Subscription;
  reviewForm: FormGroup = new FormGroup({});
  checkbox: boolean = false;
  editIconClicked: boolean = false;
  currentCustomer: Customer = null;
  productReviews: Review[];
  productRating: number;

  constructor(
    private fb: FormBuilder,
    private store: Store<fromApp.AppState>,
    private productService: ProductService, 
    private authService: AuthService,
    private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.currentCustomer = this.authService.getSignedinUser();

    this.reviewForm = this.fb.group({
      reviewRating: ['', Validators.required],
      reviewTitle: ['', Validators.required],
      reviewText: ['', Validators.required]
    })

    this.setData();
  }

  ngAfterViewChecked(){
    this.setData();
  }

  get reviewRating() {
    return this.reviewForm.get("reviewRating");
  }

  get reviewTitle() {
    return this.reviewForm.get("reviewTitle");
  }

  get reviewText() {
    return this.reviewForm.get("reviewText");
  }

  getErrorMessageRequiredReviewTitle() {
    return this.reviewForm.get("reviewTitle").hasError('required') ? 'You must enter a title' : true;
  }

  getErrorMessageRequiredReviewText() {
    return this.reviewForm.get("reviewText").hasError('required') ? 'You must enter a text' : true;
  }

  setData(){
    if(this.product != this.productService.getSelectedProduct()){
      this.product = this.productService.getSelectedProduct();

      this.imagesFormatted = [];
      this.mainProductImage = this.getImgContent("data:image/*;base64,"+this.product.images[0].picByte);
      this.formatMainProductsImages();
     
      var promo = [];
      this.product.promotions.forEach(promotion => {
        if(promotion.name == "Mix & Match")
          promo.push("Mix & Match");
      });
      if(promo.length > 0){
        this.store.dispatch(new PdpActions.FindByPromotions(promo));
        this.storeProductsFindByPromotionsSub = this.store.select('pdp')
        .subscribe(pdpState => {
          var id = this.product.id
          if(pdpState.findByPromotionsSuccess){
            this.carouselProducts = [...pdpState.productsByPromotions];
            this.carouselProducts = this.carouselProducts.filter(function(p){
              return p.id !== id;
            });
            this.formatCarouselProducts();
          }
        })
      }else{
        this.carouselProducts = [];
        this.carouselProductsFormatted = [];
      }

      this.store.dispatch(new PdpActions.GetReviewsByProductId(this.product.id));
      this.storeGetReviewsByProductIdSub =  this.storeAddEditReviewSub = this.store.select('pdp')
      .subscribe(pdpState => {
        if(pdpState.addReviewSuccess){
          this.productReviews = pdpState.getReviewsByProduct;
          this.productService.setSelectedProductReviews(pdpState.getReviewsByProduct);
        }
      });
      
    }
  }

  formatMainProductsImages(){
    var j = -1;
  
    for (var i = 0; i < this.product.images.length; i++) {
        if (i % 4 == 0) {
            j++;
            this.imagesFormatted[j] = [];
        }

        this.imagesFormatted[j].push(this.product.images[i]);
    }

  }

  formatCarouselProducts(){
    var j = -1;
  
    for (var i = 0; i < this.carouselProducts.length; i++) {
        if (i % 3 == 0) {
            j++;
            this.carouselProductsFormatted[j] = [];
        }

        this.carouselProductsFormatted[j].push(this.carouselProducts[i]);
    }

  }

  getImgContent(imgFile: string) {
    return this.productService.getImgContent(imgFile);
  }

  getCustomerImgContent(imageCustomer: ImageCustomer) {
    if(imageCustomer != null)
      return this.sanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + imageCustomer.picByte.toString());
    else
       return '../assets/images/avatar.png';
  }

  selectMainImage(image: Byte[]){
    this.mainProductImage = this.getImgContent("data:Image/*;base64," + image);
  }

  onSendReview() {
    let customer = this.checkbox ? null : this.currentCustomer;

    var review = new Review(
      this.reviewForm.value.reviewTitle, 
      this.reviewForm.value.reviewText, 
      this.reviewForm.value.reviewRating, 
      new Date(),
      customer,
      false,
      this.product
      );

    this.addEditReview(review);

  }

  onEditReview($event, review){

    if(!this.editIconClicked){
      let reviewDiv = $event.target.parentNode.parentNode.nextSibling;
      let reviewPar = $event.target.parentNode.parentNode.nextSibling.children[0];
  
      reviewPar.setAttribute('contenteditable', 'true');
      reviewPar.focus();
  
      let saveBtn = document.createElement('button');
      saveBtn.innerHTML = 'Save';
      saveBtn.onclick = (e) => {
        this.editIconClicked = false;
        this.onSaveEditedReview(e, review)
      }
  
      let cancelBtn = document.createElement('button');
      cancelBtn.innerHTML = 'Cancel';
      cancelBtn.onclick = (e) => {
        this.editIconClicked = false;
        this.onCancelEditedReview(e, review)
      }
  
      reviewDiv.append(saveBtn);
      reviewDiv.append(cancelBtn);

      this.editIconClicked = true;
    }
    
  }

  onSaveEditedReview($event, review) {
    let updatedReview;

    this.productReviews.map(arrayElement=> {
      if (arrayElement === review) {
        arrayElement = {...arrayElement, text: $event.target.previousSibling.innerHTML, edited: true}; 
        updatedReview = arrayElement;
      }
    });

    this.addEditReview(updatedReview);
  }

  onCancelEditedReview($event, review) {
    let reviewPar = $event.target.previousSibling.previousSibling;
    let reviewDiv = $event.target.parentNode;
    let cancelBtn = $event.target;
    let saveBtn = $event.target.previousSibling;

    reviewPar.setAttribute('contenteditable', 'false');
    reviewPar.innerHTML = review.text;
    reviewDiv.removeChild(saveBtn);
    reviewDiv.removeChild(cancelBtn);
  }

  onDeleteReview(review) {
    this.store.dispatch(new PdpActions.DeleteReview(review.id));
    this.storeDeleteReviewSub = this.store.select('pdp')
    .subscribe(pdpState => {
      if(pdpState.deleteReviewSuccess){
        this.productReviews = pdpState.deleteReviewResponse;
        this.productRating = this.productReviews.length > 0 ? this.productReviews[0].product.rating : 0;
        this.productService.setSelectedProductReviews(pdpState.deleteReviewResponse);
      }
    });
  }

  addEditReview(review: Review){
    this.store.dispatch(new PdpActions.AddReview(review));
    this.storeAddEditReviewSub = this.store.select('pdp')
    .subscribe(pdpState => {
      if(pdpState.addReviewSuccess){
        this.productReviews = pdpState.getReviewsByProduct;
        this.productService.setSelectedProductReviews(pdpState.getReviewsByProduct);
        this.productRating = this.productReviews.length > 0 ? this.productReviews[0].product.rating : 0;

        this.reviewForm.reset();
      }
    });
  }

  onClickAnonymous(checkbox: MatCheckboxChange) {
    this.checkbox = checkbox.checked;
  }

  ngOnDestroy(): void {
    this.storeProductsFindByPromotionsSub?.unsubscribe();
    this.storeAddEditReviewSub?.unsubscribe();
    this.storeGetReviewsByProductIdSub?.unsubscribe();
  }
}