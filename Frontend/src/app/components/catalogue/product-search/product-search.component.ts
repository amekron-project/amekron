import { Component, ElementRef, HostListener, OnInit, QueryList, Renderer2, ViewChildren } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { Subscription } from 'rxjs';
import { Product } from '../../../models/product.model';
import { ProductService } from '../../../services/product.service';

@Component({
  selector: 'app-product-search',
  templateUrl: './product-search.component.html',
  styleUrls: ['./product-search.component.less']
})
export class ProductSearchComponent implements OnInit {
  products: Product[] = [];
  currentProductsToShow = [];
  productsNumber: number;
  noProdAvailable = false;
  messages: any[] = [];
  @ViewChildren('paginator') paginator: QueryList<MatPaginator>;

  constructor(
    private productService: ProductService,
    private elem: ElementRef,
    private renderer: Renderer2,
  ) {}

  ngOnInit() {
    this.getProducts();
  }

  ngAfterViewChecked(){
    this.getProducts();
  }

  getProducts(){
    this.products = [];
    let productResult = this.productService.getResultId();
    let productsResult = this.productService.getResultName();

    if(productResult != null && productResult != undefined){
      if(productResult != this.products[0]){
        if(this.paginator != undefined && this.paginator.first != undefined)
          this.paginator.first.pageIndex = 0;
        this.products = [...this.products, productResult];
        this.currentProductsToShow = this.products.slice(0,6);
        this.productsNumber = this.products.length;
        this.noProdAvailable = false;
      }
    }
    else
      if(productsResult != undefined && productsResult.length > 0){
        if(productsResult != this.products){
          if(this.paginator != undefined && this.paginator.first != undefined)
            this.paginator.first.pageIndex = 0;
          this.products = productsResult;
          this.currentProductsToShow = this.products.slice(0,6);
          this.productsNumber = this.products.length;
          this.noProdAvailable = false;
        }
      }
      else {
        this.noProdAvailable = true;
      }
  }

  @HostListener('window:resize') onResize() {
    let resizeTimer;
    clearTimeout(resizeTimer);

    resizeTimer = setTimeout(() => {
      this.productService.calculateHeights(this.elem, this.renderer);
    }, 250);
  }

  onPageChange($event) {
    this.currentProductsToShow = this.products.slice(
      $event.pageIndex * $event.pageSize,
      $event.pageIndex * $event.pageSize + $event.pageSize
    );
  }

}
