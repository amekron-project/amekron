import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/services/auth.service";

@Component({
    selector: 'app-email-sent',
    templateUrl: './email-sent.component.html',
    styleUrls: ['./email-sent.component.less']
  })
export class EmailSentComponent implements OnInit{

  emailSentTo: string;

  constructor(private authService: AuthService){}

  ngOnInit(): void {
     this.emailSentTo = this.authService.getSignedinUser().email;
  }
    
}