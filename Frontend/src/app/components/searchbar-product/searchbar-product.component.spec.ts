import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchbarProductComponent } from './searchbar-product.component';

describe('SearchbarProductComponent', () => {
  let component: SearchbarProductComponent;
  let fixture: ComponentFixture<SearchbarProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchbarProductComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchbarProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
