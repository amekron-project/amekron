import { Component, ElementRef, HostListener, OnDestroy, Renderer2, ViewChild } from '@angular/core';
import * as fromApp from '../../store/app.reducer';
import * as SearchActions from './store/search.actions';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ProductService } from '../../services/product.service';
import { Product } from '../../models/product.model';

@Component({
  selector: 'app-searchbar-product',
  templateUrl: './searchbar-product.component.html',
  styleUrls: ['./searchbar-product.component.less']
})
export class SearchbarProductComponent implements  OnDestroy{
  storeProductsFindByIdSubEnter: Subscription;
  storeProductsFindByNameSubEnter: Subscription;
  storeProductsFindByIdSub: Subscription;
  storeProductsFindByNameSub: Subscription;
  productsResultDisplay: Product[] = [];
  input = '';
  enterPressed = false;
  @ViewChild('searchbarEl') searchbarEl: ElementRef;

  constructor(
    private store: Store<fromApp.AppState>, 
    private _snackBar: MatSnackBar,
    private router: Router,
    private productService: ProductService,
    private elem: ElementRef,
    private renderer: Renderer2,
  ) { 
    this.renderer.listen('window', 'click',(e:Event)=>{
      if(e.target !== this.searchbarEl.nativeElement){
        this.enterPressed = true;
      }
    });
  }

  ngAfterViewChecked() {
    this.elem.nativeElement.querySelectorAll('.result-display').forEach(element => {
      if(this.input == '' || this.productsResultDisplay.length == 0 || this.enterPressed) {
        this.renderer.setStyle(element, 'border', 'none');
        this.renderer.setStyle(element, 'overflow', 'hidden');
        this.renderer.setStyle(element, 'display', 'none');
      }
      else {
        this.renderer.setStyle(element, 'border', '1px solid black');
        this.renderer.setStyle(element, 'overflow', 'scroll');
        this.renderer.setStyle(element, 'display', 'block');
        this.calculateResultWidth();
      }
    })
  }

  onSelectProduct(product: Product) {
    this.productService.setSelectedProduct(product);
    this.router.navigate(['../catalogue/product_details', `${product.name}`]);
  }

  onKeyUpEvent(event: any) {
    if(event.key != 'Enter')
      this.enterPressed = false;

    if(!isNaN(+event.target.value)){
      this.store.dispatch(new SearchActions.SearchById(event.target.value))
      this.storeProductsFindByIdSub = this.store.select('search')
      .subscribe(searchState => {
        this.productsResultDisplay = [];
        if(searchState.searchByIdSuccess){
          this.productsResultDisplay.push(searchState.resultById);
        }
      })
    } else if(typeof event.target.value == 'string') {
      this.store.dispatch(new SearchActions.SearchByName(event.target.value))
      this.storeProductsFindByNameSub = this.store.select('search')
      .subscribe(searchState => {
        this.productsResultDisplay = [];
        if(searchState.searchByNameSuccess){
          this.productsResultDisplay = searchState.resultByName;
        }
      })
    }
  }

  onKeyEnterEvent(event: any){
    this.productsResultDisplay = [];
    this.enterPressed = true;

    if(!isNaN(+event.target.value)){
      this.store.dispatch(new SearchActions.SearchById(event.target.value))
      this.storeProductsFindByIdSubEnter = this.store.select('search')
      .subscribe(searchState => {
        if(searchState.searchByIdSuccess){
          this.productService.updateResultName(null);
          this.productService.updateResultId(searchState.resultById);
          this.router.navigate(['catalogue/results']);
        } else {
          this.productService.updateResultId(null);
          this.router.navigate(['catalogue/results'])
        }
      },
      (error: HttpErrorResponse) => {
          var message = "Ops! There was a problem with searching products. Please contact us at ###";
          var action = "Close";
          this._snackBar.open(message, action);
      })

      this.storeProductsFindByIdSubEnter.unsubscribe();

    } else if(typeof event.target.value == 'string') {
      this.store.dispatch(new SearchActions.SearchByName(event.target.value))
      this.storeProductsFindByNameSubEnter = this.store.select('search')
      .subscribe(searchState => {
        if(searchState.searchByNameSuccess){
          this.productService.updateResultId(null);
          this.productService.updateResultName(searchState.resultByName);
          this.router.navigate(['catalogue/results']);
        } else {
          this.productService.updateResultName(null);
          this.router.navigate(['catalogue/results'])
        }
      },
      (error: HttpErrorResponse) => {
          var message = "Ops! There was a problem with searching products. Please contact us at ###";
          var action = "Close";
          this._snackBar.open(message, action);
      })

      this.storeProductsFindByNameSubEnter.unsubscribe();
    }
  }

  @HostListener('window:resize') onResize() {
    let resizeTimer;
    clearTimeout(resizeTimer);

    resizeTimer = setTimeout(() => {
      this.calculateResultWidth();
    }, 250);
  }

  calculateResultWidth() {
    const width =  document.getElementById('input').offsetWidth;
    this.elem.nativeElement.querySelectorAll('.result-display').forEach(element => {
      element.style.width = `${width}px`;
    })
  }

  getImgContent(imgFile: string) {
    return this.productService.getImgContent(imgFile);
  }

  ngOnDestroy(): void {
    this.storeProductsFindByIdSub.unsubscribe();
    this.storeProductsFindByNameSub.unsubscribe();
  }

}
