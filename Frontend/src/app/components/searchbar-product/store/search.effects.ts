import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Store } from "@ngrx/store";
import * as fromApp from '../../../store/app.reducer';
import * as SearchActions from './search.actions';
import { environment } from "src/environments/environment";
import { catchError, map, of, switchMap, withLatestFrom } from "rxjs";
import { Product } from "src/app/models/product.model";

@Injectable()
export class SearchEffects {
    constructor(
        private actions$: Actions,
        private http: HttpClient,
        private store: Store<fromApp.AppState>
    ){}

    @Effect()
    searchById = this.actions$.pipe(
        ofType(SearchActions.SEARCH_BY_ID),
        withLatestFrom(this.store.select('search')),
        switchMap(([actionData, searchState]) => {
            return this.http.get<Product>(`${environment.apiBaseUrl}/product/findById/${searchState.id}`)
            .pipe(
                map(resData => {return new SearchActions.SearchByIdSuccess(resData);}),
                catchError(errorRes => {
                    return of(new SearchActions.SearchByIdFail())
                })
            )
        })
    )

    @Effect()
    searchByName = this.actions$.pipe(
        ofType(SearchActions.SEARCH_BY_NAME),
        withLatestFrom(this.store.select('search')),
        switchMap(([actionData, searchState]) => {
            return this.http.post<Product[]>(`${environment.apiBaseUrl}/product/findByName/`, searchState.name)
            .pipe(
                map(resData => {return new SearchActions.SearchByNameSuccess(resData);}),
                catchError(errorRes => {
                    return of(new SearchActions.SearchByNameFail())
                })
            )
        })
    )

}
