import { Product } from 'src/app/models/product.model';
import * as SearchActions from './search.actions';

export interface State {
    resultById: Product,
    resultByName: Product[],
    id: number,
    name: string,
    searchByIdSuccess: boolean,
    searchByNameSuccess: boolean
}
  
const initialState: State = {
    resultById: null,
    resultByName: null,
    id: null,
    name: null,
    searchByIdSuccess: false,
    searchByNameSuccess: false
}
  
  export function searchReducer(state = initialState, action: SearchActions.SearchActions){
    switch (action.type){
        case SearchActions.SEARCH_BY_ID:
            const id = action.payload;
            return {
                ...state,
                id: id
            }
  
        case SearchActions.SEARCH_BY_ID_SUCCESS:
          const resultById = action.payload;
            return {
                ...state,
                resultById: resultById,
                searchByIdSuccess: true
            }
  
        case SearchActions.SEARCH_BY_ID_FAIL:
            return {
                ...state,
                searchByIdSuccess: false
            }
  
        case SearchActions.SEARCH_BY_NAME:
            const name = action.payload;
            return {
                ...state,
                name: name
            }
      
        case SearchActions.SEARCH_BY_NAME_SUCCESS:
            const resultByName = action.payload;
            return {
                ...state,
                resultByName: resultByName,
                searchByNameSuccess: true
        }
      
        case SearchActions.SEARCH_BY_NAME_FAIL:
            return {
                ...state,
                searchByNameSuccess: false
            }

        default:
            return state;
    }
  }
  