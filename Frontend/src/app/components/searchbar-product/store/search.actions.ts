import { Action } from "@ngrx/store";
import { Product } from "src/app/models/product.model";

export const SEARCH_BY_ID_ENTER = '[Search] SEARCH_BY_ID_ENTER';
export const SEARCH_BY_ID_FAIL_ENTER = '[Search] SEARCH_BY_ID_FAIL_ENTER';
export const SEARCH_BY_ID_SUCCESS_ENTER = '[Search] SEARCH_BY_ID_SUCCESS_ENTER';
export const SEARCH_BY_NAME_ENTER = '[Search] SEARCH_BY_NAME_ENTER';
export const SEARCH_BY_NAME_FAIL_ENTER = '[Search] SEARCH_BY_ID_FAIL_ENTER';
export const SEARCH_BY_NAME_SUCCESS_ENTER = '[Search] SEARCH_BY_NAME_SUCCESS_ENTER';
export const SEARCH_BY_ID = '[Search] SEARCH_BY_ID';
export const SEARCH_BY_ID_FAIL = '[Search] SEARCH_BY_ID_FAIL';
export const SEARCH_BY_ID_SUCCESS = '[Search] SEARCH_BY_ID_SUCCESS';
export const SEARCH_BY_NAME = '[Search] SEARCH_BY_NAME';
export const SEARCH_BY_NAME_FAIL = '[Search] SEARCH_BY_ID_FAIL';
export const SEARCH_BY_NAME_SUCCESS = '[Search] SEARCH_BY_NAME_SUCCESS';

export class SearchById implements Action {
    readonly type = SEARCH_BY_ID;

    constructor(public payload: number){}
}

export class SearchByIdFail implements Action {
    readonly type = SEARCH_BY_ID_FAIL;
}

export class SearchByIdSuccess implements Action {
    readonly type = SEARCH_BY_ID_SUCCESS;

    constructor(public payload: Product){}
}

export class SearchByName implements Action {
    readonly type = SEARCH_BY_NAME;

    constructor(public payload: string){}
}

export class SearchByNameFail implements Action {
    readonly type = SEARCH_BY_NAME_FAIL;
}

export class SearchByNameSuccess implements Action {
    readonly type = SEARCH_BY_NAME_SUCCESS;

    constructor(public payload: Product[]){}
}   

export class SearchByIdEnter implements Action {
    readonly type = SEARCH_BY_ID_ENTER;

    constructor(public payload: number){}
}

export class SearchByIdFailEnter implements Action {
    readonly type = SEARCH_BY_ID_FAIL_ENTER;
}

export class SearchByIdSuccessEnter implements Action {
    readonly type = SEARCH_BY_ID_SUCCESS_ENTER;

    constructor(public payload: Product){}
}

export class SearchByNameEnter implements Action {
    readonly type = SEARCH_BY_NAME_ENTER;

    constructor(public payload: string){}
}

export class SearchByNameFailEnter implements Action {
    readonly type = SEARCH_BY_NAME_FAIL_ENTER;
}

export class SearchByNameSuccessEnter implements Action {
    readonly type = SEARCH_BY_NAME_SUCCESS_ENTER;

    constructor(public payload: Product[]){}
}

export type SearchActions = 
| SearchById
| SearchByIdFail
| SearchByIdSuccess
| SearchByName
| SearchByNameFail
| SearchByNameSuccess
| SearchByIdEnter
| SearchByIdFailEnter
| SearchByIdSuccessEnter
| SearchByNameEnter
| SearchByNameFailEnter
| SearchByNameSuccessEnter;