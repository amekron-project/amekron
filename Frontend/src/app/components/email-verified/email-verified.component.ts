import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Store } from "@ngrx/store";
import * as fromApp from '../../store/app.reducer';
import * as AuthActions from '../login/store/auth.actions';

@Component({
    selector: 'app-email-verified',
    templateUrl: './email-verified.component.html',
    styleUrls: ['./email-verified.component.less']
  })
export class EmailVerifiedComponent implements OnInit{

  emailSentTo: string;

  constructor( 
    private store: Store<fromApp.AppState>,
    private route: ActivatedRoute){}

  ngOnInit(): void {
    const token = this.route.snapshot.paramMap.get('confirmation-token');

    this.store.dispatch(new AuthActions.ConfirmEmail(token));
  }
    
}