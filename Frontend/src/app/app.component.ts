import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';
import { CartService } from './services/cart.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit{
  loggedIn = false;
  cartItemNumber: number;

  constructor(
    private authService: AuthService, 
    private cartService:  CartService
    ) {}

  ngOnInit(): void {
    this.authService.loggedIn.subscribe(res => {
      if(res == this.authService.isUserSignedin())
        this.loggedIn = res;
    })

    if(this.authService.isUserSignedin())
      this.loggedIn = true;

    this.cartItemNumber = this.cartService.getCartItemsNo();
    this.cartService.cartItemNumber?.subscribe(itemNo => {
      if(itemNo != null) {
        this.cartItemNumber = itemNo
      }
    })
  }

  onLogout(){
    this.authService.onLogout();
    this.loggedIn = false;
  }
}
