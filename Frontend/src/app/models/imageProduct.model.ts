import { Byte } from "@angular/compiler/src/util";
import { Product } from "./product.model";

export class ImageProduct {
  id: number;
  product: Product;
  name: string;
  type: string;
  picByte: Byte[];
}
