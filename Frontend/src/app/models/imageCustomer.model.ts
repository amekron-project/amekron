import { Byte } from "@angular/compiler/src/util";
import { Customer } from "./customer.model";

export class ImageCustomer {
  id: number;
  customer: Customer;
  name: string;
  type: string;
  picByte: Byte[];
}
