import { ImageProduct } from "./imageProduct.model";
import { Promotion } from "./promotion.model";
import { Review } from "./review.model";

export class Product {
  constructor(
    public id: number,
    public name: string,
    public quantity: number,
    public price: number,
    public discountedPrice: number,
    public description: string,
    public rating: number,
    public promotions: Promotion[],
    public images: ImageProduct[],
    public creationDate: Date,
    public reviews: Review[]
    ){}
}
