import { Product } from "./product.model";

export class Promotion {
  constructor(
    public id: Number,
    public name: string,
    public start_time: Date,
    public end_time: Date,
    public products: Product[],
    public description: string,
    public type: string
  ){}
}
