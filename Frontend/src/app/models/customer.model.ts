import { ImageCustomer } from "./imageCustomer.model";

export class Customer {
  constructor(
    public id: number, 
    public firstName: string, 
    public lastName: string, 
    public email: string, 
    public password: string, 
    public address: string, 
    public country: string,
    public city: string,
    public zipCode: string,
    public phone: string,
    public imageCustomer: ImageCustomer){}
  }