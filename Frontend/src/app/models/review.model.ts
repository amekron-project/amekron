import { Customer } from "./customer.model";
import { Product } from "./product.model";

export class Review {
    constructor(
        public title: String,
        public text: String,
        public rating: number,
        public creationDate: Date,
        public customer: Customer,
        public edited: boolean,
        public product: Product
    ){}
}