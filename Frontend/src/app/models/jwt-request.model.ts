export class JwtRequest {
    constructor(
        public firstName: string, 
        public lastName: string, 
        public email: string, 
        public password: string, 
        public address?: string, 
        public city?: string,
        public country?: string,
        public zipCode?: string,
        public phone?: string,
        public roles?: string[]
    ){}
}