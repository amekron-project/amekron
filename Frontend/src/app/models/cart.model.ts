import { Product } from "./product.model";

export class Cart {
    constructor(
      public id: number,
      public products: Product[],
      public quantities: Map<number, number>,
      public productsTotalPrice: Map<number, number>,
      public subtotalPrice: number,
      public discount: number,
      public totalPrice: number,
      public mixMatch: boolean,
      public deliveryTax: number
      ){}
}
