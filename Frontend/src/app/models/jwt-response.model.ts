import { Customer } from "./customer.model";

export class JwtResponse {
    constructor(
        public customer: Customer,
        public token: string,
        public roles?: string[]
    ){}
}