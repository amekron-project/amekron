import { Cart } from "./cart.model";
import { Customer } from "./customer.model";

export class Order {
    constructor(
        public id: number,
        public cart: Cart,
        public customer: Customer
    ){}
}