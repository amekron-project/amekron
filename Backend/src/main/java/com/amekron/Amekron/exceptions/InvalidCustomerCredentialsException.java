package com.amekron.Amekron.exceptions;

public class InvalidCustomerCredentialsException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public InvalidCustomerCredentialsException(String msg) {
        super(msg);
    }

}