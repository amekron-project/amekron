package com.amekron.Amekron.exceptions;

public class DisabledCustomerException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public DisabledCustomerException(String msg) {
        super(msg);
    }

}