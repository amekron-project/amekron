package com.amekron.Amekron.repos;
import com.amekron.Amekron.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface ProductRepo extends JpaRepository<Product, Long> {
    void deleteProductById(Long id);

    @Query(value = "select * from Product", nativeQuery = true)
    Optional<Set<Product>> getAllProducts();

    @Query(value = "select * from Product where Product.name like %?1%", nativeQuery = true)
    Optional<Set<Product>> getProductsByName(String name);

    @Query(value = "select * from Product where Product.id=?1", nativeQuery = true)
    Optional<Product> getProductById(Long id);

    @Query(value = "select * from product inner join product_promotion on product.id = product_promotion.product_id inner join promotion on product_promotion.promotion_id = promotion.id where promotion.name=?1", nativeQuery = true)
    Optional<List<Product>> getProductsByPromotion(String promotion);

    @Query(value = "select * from Product where Product.category=?1", nativeQuery = true)
    Optional<List<Product>> getProductsByCategory(String category);
}
