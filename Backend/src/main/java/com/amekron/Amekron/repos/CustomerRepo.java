package com.amekron.Amekron.repos;

import com.amekron.Amekron.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface CustomerRepo extends JpaRepository<Customer, Long> {

    void deleteCustomerById(Long id);

    @Query(value = "select * from Customer where Customer.customer_id=?1", nativeQuery = true)
    Optional<Customer> getCustomerById(Long email);

    @Query(value = "select * from Customer where Customer.email=?1 and Customer.password=?2", nativeQuery = true)
    Optional<Customer> getCustomerByEmailAndPassword(String email, String password);

    @Query(value = "select * from Customer where Customer.email=?1", nativeQuery = true)
    Optional<Customer> getCustomerByEmail(String email);
}
