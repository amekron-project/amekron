package com.amekron.Amekron.repos;
import com.amekron.Amekron.models.Product;
import com.amekron.Amekron.models.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ReviewRepo extends JpaRepository<Review, Long> {
    @Query(value = "select * from review where review.product_id=?1", nativeQuery = true)
    Optional<List<Review>> getByProductById(Long id);

    @Query(value = "select * from review where review.id=?1", nativeQuery = true)
    Optional<Review> getReviewById(Long id);
}
