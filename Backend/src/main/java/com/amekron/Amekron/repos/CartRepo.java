package com.amekron.Amekron.repos;

import com.amekron.Amekron.models.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CartRepo extends JpaRepository<Cart, Long> {
    void deleteCartById(Long id);

    @Query(value = "select * from Cart where Cart.cart_id=?1", nativeQuery = true)
    Cart getCartById(Long id);
}
