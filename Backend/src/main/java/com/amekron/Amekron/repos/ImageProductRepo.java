package com.amekron.Amekron.repos;

import com.amekron.Amekron.models.ImageProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ImageProductRepo extends JpaRepository<ImageProduct, Long> {
    @Query(value = "select * from image_product where image_product.product_id=?1", nativeQuery = true)
    Optional<List<ImageProduct>> findByProductId(Long id);

    void deleteImageById(Long id);
}
