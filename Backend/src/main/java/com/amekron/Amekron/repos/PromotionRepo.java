package com.amekron.Amekron.repos;

import com.amekron.Amekron.models.Product;
import com.amekron.Amekron.models.Promotion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface PromotionRepo extends JpaRepository<Promotion, Long> {
    void deletePromotionById(Long id);

    @Query(value = "select * from Promotion", nativeQuery = true)
    Optional<List<Promotion>> getAllPromotions();

    @Query(value = "select * from Promotion where Promotion.id=?1", nativeQuery = true)
    Optional<Promotion> getPromotionById(Long id);
}
