package com.amekron.Amekron.repos;
import com.amekron.Amekron.models.ImageCustomer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ImageCustomerRepo extends JpaRepository<ImageCustomer, Long> {
    @Query(value = "select * from image_customer where image_customer.customer_id=?1", nativeQuery = true)
    Optional<ImageCustomer> findByCustomerId(Long customerId);

    @Modifying
    @Query(value = "delete from image_customer where image_customer.customer_id=?1", nativeQuery = true)
    void deleteImageByCustomerId(Long id);
}
