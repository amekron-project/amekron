package com.amekron.Amekron.schedulers;

import com.amekron.Amekron.services.PromotionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class PromotionDeleteScheduler {
    private final PromotionService promotionService;

    @Autowired
    public PromotionDeleteScheduler(PromotionService promotionService) {
        this.promotionService = promotionService;
    }

    @Scheduled(cron = "0 * 9 * * ?")
    public void cronJobSch() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);

        promotionService.getAllPromotions().forEach(promotion -> {
            if(promotion.getEnd_time().after(new Date())) {
                promotionService.deletePromotionById(promotion.getId());
            }
        });

        System.out.println("Java cron job to delete expired promotions:: " + strDate);
    }
}
