package com.amekron.Amekron.resources;

import com.amekron.Amekron.emailVerification.ConfirmationToken;
import com.amekron.Amekron.emailVerification.ConfirmationTokenRepo;
import com.amekron.Amekron.exceptions.ObjectNotFoundException;
import com.amekron.Amekron.services.EmailSenderService;
import com.amekron.Amekron.exceptions.DisabledCustomerException;
import com.amekron.Amekron.exceptions.InvalidCustomerCredentialsException;
import com.amekron.Amekron.jwt.JwtRequest;
import com.amekron.Amekron.jwt.JwtResponse;
import com.amekron.Amekron.models.*;
import com.amekron.Amekron.services.CustomerService;
import com.amekron.Amekron.utilities.ImageCustomerService;
import com.amekron.Amekron.services.OrderService;
import com.amekron.Amekron.services.ReviewService;
import com.amekron.Amekron.utilities.JwtUtil;
import com.amekron.Amekron.utilities.UtilityClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/customer")
@CrossOrigin(value = "http://localhost:4200")
public class CustomerResource {
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private ConfirmationTokenRepo confirmationTokenRepo;
    @Autowired
    private EmailSenderService emailSenderService;
    @Autowired
    private ImageCustomerService imageCustomerService;
    @Autowired
    private ReviewService reviewService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("/signin")
    public ResponseEntity<JwtResponse> generateJwtToken(@RequestBody Customer customer) {
        JwtResponse jwtResponse = createJwtResponse(customer, false);

        return new ResponseEntity<JwtResponse>(jwtResponse, HttpStatus.OK);
    }

    @PostMapping("/signup")
    public ResponseEntity<JwtResponse> signup(@RequestBody JwtRequest jwtRequest){
        Customer newCustomer = customerService.saveCustomer(jwtRequest);
        JwtResponse jwtResponse = createJwtResponse(newCustomer, true);

        ConfirmationToken confirmationToken = new ConfirmationToken(newCustomer);

        confirmationTokenRepo.save(confirmationToken);

        emailSenderService.sendEmail(
                newCustomer.getEmail(),
                "Email verification",
                "You just registered on Amekron e-commerce website for the best 3D prints. " +
                        "\nTo confirm your email address, please click here : " + "http://localhost:4200/email-verified/"
                        + confirmationToken.getConfirmationToken());


        return new ResponseEntity<>(jwtResponse, HttpStatus.OK);
    }

    @PostMapping("/confirm-account")
    public ResponseEntity<?> confirmCustomerEmail(@RequestBody String token)
    {

        ConfirmationToken confirmationToken = confirmationTokenRepo.findByConfirmationToken(token);

        if(token != null) {
            Customer customer = customerService.getCustomerByEmail(confirmationToken.getCustomer().getEmail());
            customer.setIsEnabled(true);
            customerService.updateCustomer(customer);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/change-password")
    public ResponseEntity<?> changeCustomerPassword(@RequestBody Customer customer){
//        ConfirmationToken confirmationToken = new ConfirmationToken(customer);
//
//        confirmationTokenRepo.save(confirmationToken);

        emailSenderService.sendEmail(
                customer.getEmail(),
                "Password change request",
                "You requested to change your password.\n " +
                        "If it was not you who requested this, you can simply ignore this mail.\n" +
                        "If you would still like to proceed with password update, please click the link: "
                        + "http://localhost:4200/my-account/change-password/");


        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<Customer> getCustomerById (@PathVariable("id") Long id) {
        Customer customerResponse = customerService.getCustomerById(id);
        retrieveImage(customerResponse);
        return new ResponseEntity<>(customerResponse, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<Customer> updateCustomer(@RequestBody Customer customer) {
        Customer existentCustomer = customerService.getCustomerByEmail(customer.getEmail());

        List<Review> productReviews = reviewService.getReviewsByProductId(customer.getId());
        productReviews.forEach(review -> {
            review.setCustomer(existentCustomer);
            reviewService.addReview(review);
        });

        customer.setReviews(productReviews);
        customer.setIsEnabled(existentCustomer.getIsEnabled());

        List<Order> orders = orderService.getOrdersByCustomerId(customer.getId());
        orders.forEach(order -> {
            order.setCustomer(customer);
            orderService.addOrder(order);
        });

        customer.setOrders(orders);

        Optional<ImageCustomer> imageCustomer = imageCustomerService.findByCustomerId(customer.getId());

        if(imageCustomer.equals(Optional.empty())){
            customer.setImageCustomer(null);
        } else {
            customer.setImageCustomer(imageCustomer.get());
        }

        //TO DO --- encode password when it is updated
        //customer.setPassword(passwordEncoder.encode(customer.getPassword()));

        Customer updateCustomer = customerService.updateCustomer(customer);
        retrieveImage(updateCustomer);

        return new ResponseEntity<>(updateCustomer, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable("id") Long id) {
        customerService.deleteCustomer(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private JwtResponse createJwtResponse(Customer customer, Boolean signup) {
        Authentication authentication = null;

        if(!signup) {
            try {
                authentication = authenticationManager
                        .authenticate(new UsernamePasswordAuthenticationToken(customer.getEmail(), customer.getPassword()));
            } catch (DisabledException e) {
                throw new DisabledCustomerException("User Inactive");
            } catch (BadCredentialsException e) {
                throw new InvalidCustomerCredentialsException("Invalid Credentials");
            }
        }

        Set<String> roles;
        Customer existentCustomer = customerService.getCustomerByEmail(customer.getEmail());

        if (authentication != null) {
            User user = (User) authentication.getPrincipal();
            roles = user.getAuthorities().stream().map(r -> r.getAuthority()).collect(Collectors.toSet());
        } else {
            roles = existentCustomer.getCustomerRoles().stream().map(r -> r.getRole()).collect(Collectors.toSet());
        }

        String token = jwtUtil.generateToken(existentCustomer.getEmail());
        JwtResponse jwtResponse = new JwtResponse();

        Customer jwtResponseCustomer = customerService.getCustomerByEmail(existentCustomer.getEmail());
        jwtResponse.setCustomer(jwtResponseCustomer);
        retrieveImage(jwtResponseCustomer);

        jwtResponse.setToken(token);
        jwtResponse.setRoles(roles.stream().collect(Collectors.toList()));
        return jwtResponse;

    }

    private void retrieveImage(Customer customer){
        //a customer can only have one image
        final Optional<ImageCustomer> retrievedImage = imageCustomerService.findByCustomerId(customer.getId());

        AtomicReference<ImageCustomer> imageCustomer = new AtomicReference<>();
        if (retrievedImage != null) {
            retrievedImage.stream().forEach(imageFile -> {
                imageCustomer.set(
                        new ImageCustomer(
                                imageFile.getName(),
                                imageFile.getType(),
                                UtilityClass.decompressBytes(imageFile.getPicByte())
                        )
                );
            });
        };

        customer.setImageCustomer(imageCustomer.get());
    }


}
