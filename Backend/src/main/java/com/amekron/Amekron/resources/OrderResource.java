package com.amekron.Amekron.resources;

import com.amekron.Amekron.services.EmailSenderService;
import com.amekron.Amekron.models.Order;
import com.amekron.Amekron.models.Review;
import com.amekron.Amekron.services.OrderService;
import com.amekron.Amekron.services.ProductService;
import com.amekron.Amekron.services.ReviewService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;


@RestController
@RequestMapping("/order")
public class OrderResource {
    @Autowired
    private EmailSenderService emailSenderService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ReviewService reviewService;

    @PostMapping("/add")
    public ResponseEntity<?> addOrder(@RequestBody @NotNull  Order order) {
        if(order.getCustomer().getId() == null) {
            order.setFirstName(order.getCustomer().getFirstName());
            order.setLastName(order.getCustomer().getLastName());
            order.setEmail(order.getCustomer().getEmail());
            order.setPhone(order.getCustomer().getPhone());
            order.setAddress(order.getCustomer().getAddress());
            order.setCity(order.getCustomer().getCity());
            order.setCountry(order.getCustomer().getCountry());
            order.setZipCode(order.getCustomer().getZipCode());
            order.setCustomer(null);
        }

        Order newOrder = orderService.addOrder(order);

        order.getCart().getProducts().forEach(p -> {
            //key is product is, value is qty ordered
            order.getCart().getQuantities().forEach((key, value) -> {
                if(Objects.equals(key, p.getId())) {
                    p.setQuantity(p.getQuantity() - value);
                    List<Review> productReviews = reviewService.getReviewsByProductId(p.getId());
                    p.setReviews(productReviews);
                    productService.updateProduct(p);
                }
            });
        });

        emailSenderService.sendEmail(
                newOrder.getEmail() != null ? newOrder.getEmail() : order.getCustomer().getEmail(),
                "Amekron 3D prints order sent",
                "Order " + newOrder.getId() + " was placed succesfully.\n" +
                "Transportation can take up to 7 days, depending on the location.\n" +
                "Thank you for your order! We will contact you further once the order status updates!");

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<List<Order>> getOrdersById (@PathVariable("id") Long id) {
        List<Order> orderResponse = orderService.getOrdersByCustomerId(id);
        return new ResponseEntity<>(orderResponse, HttpStatus.OK);
    }
}
