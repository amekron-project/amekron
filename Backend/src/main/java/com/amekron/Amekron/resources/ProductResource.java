package com.amekron.Amekron.resources;
import com.amekron.Amekron.models.*;
import com.amekron.Amekron.services.*;
import com.amekron.Amekron.utilities.ImageCustomerService;
import com.amekron.Amekron.utilities.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@RestController
@RequestMapping("/product")
public class ProductResource {
    private ProductService productService;
    @Autowired
    private ImageCustomerService imageCustomerService;
    @Autowired
    private ReviewService reviewService;
    @Autowired
    CustomerService customerService;
    @Autowired
    ImageProductService imageProductService;

    @Autowired
    public ProductResource(ProductService productService) {
        this.productService = productService;
    }


    @PostMapping("/findByName")
    public ResponseEntity<Set<Product>> getProductsByName (@RequestBody @NotNull String name) {
        Set<Product> productsResponse = productService.getProductsByName(name);
        productsRetrieveImages(productsResponse);
        return new ResponseEntity<>(productsResponse, HttpStatus.OK);
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<Product> getProductById (@PathVariable("id") Long id) {
        Product productsResponse = productService.getProductById(id);
        retrieveProductImages(productsResponse);
        return new ResponseEntity<>(productsResponse, HttpStatus.OK);
    }

    @PostMapping("/findByCategories")
    public ResponseEntity<Set<Product>> getProductsByCategories(@RequestBody @NotNull List<String> categories) {
        Set<Product> finalProductsResponse = new HashSet<>();
        for (String category : categories) {
            List<Product> productsResponse = productService.getProductsByCategory(category);
            finalProductsResponse.addAll(productsResponse);
        }

        productsRetrieveImages(finalProductsResponse);

        return new ResponseEntity<>(finalProductsResponse, HttpStatus.OK);
    }


    @PostMapping("/findByPromotions")
    public ResponseEntity<Set<Product>> getProductsByPromotions(@RequestBody @NotNull List<String> promotions) {
        Set<Product> finalProductsResponse = new HashSet<>();
        for (String promotion : promotions) {
            List<Product> productsResponse = productService.getProductsByPromotion(promotion);
            finalProductsResponse.addAll(productsResponse);
        }

        productsRetrieveImages(finalProductsResponse);

        return new ResponseEntity<>(finalProductsResponse, HttpStatus.OK);
    }

    @PostMapping("/findByCategoriesAndPromotions")
    public ResponseEntity<Set<Product>> getProductsByCategoriesAndPromotions(@RequestBody @NotNull PromotionCategoryObj obj) {
        List<String> categories = obj.getSelectedCategories();
        List<String> promotions = obj.getSelectedPromotions();

        Set<Product> categoriesProductsResponse = new HashSet<>();
        categoriesProductsResponse = getProductsByCategories(categories).getBody();

        Set<Product> promotionsProductsResponse = new HashSet<>();
        promotionsProductsResponse = getProductsByPromotions(promotions).getBody();

        if(!promotionsProductsResponse.isEmpty() && !categoriesProductsResponse.isEmpty()) {
            Set<Product> common = new HashSet<>(categoriesProductsResponse);
            common.retainAll(promotionsProductsResponse);
            productsRetrieveImages(common);
            return new ResponseEntity<>(common, HttpStatus.OK);
        }
        else if(!categoriesProductsResponse.isEmpty()) {
            productsRetrieveImages(categoriesProductsResponse);
            return new ResponseEntity<>(categoriesProductsResponse, HttpStatus.OK);
        }
        else {
            productsRetrieveImages(promotionsProductsResponse);
            return new ResponseEntity<>(promotionsProductsResponse, HttpStatus.OK);
        }
    }

    @GetMapping("/findAll")
    public ResponseEntity<Set<Product>> getAllProducts() {
        Set<Product> productsResponse = productService.getAllProducts();
        productsRetrieveImages(productsResponse);
        return new ResponseEntity<>(productsResponse, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<Product> addProduct(@RequestBody Product product) {
        calculateProductDiscountedPrice(product);
        Product newProduct = productService.addProduct(product);
        return new ResponseEntity<>(newProduct, HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<Product> updateProduct(@RequestBody Product product) {
        List<Review> productReviews = reviewService.getReviewsByProductId(product.getId());
        productReviews.forEach(review -> {
            review.setProduct(product);
            reviewService.addReview(review);
        });

        product.setReviews(productReviews);
        calculateProductDiscountedPrice(product);
        Product existentProduct = productService.updateProduct(product);

        return new ResponseEntity<>(existentProduct, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable("id") Long id) {
        Product product = productService.getProductById(id);
        for (ImageProduct image : product.getImages()) {
            if (image.getProduct() != null) {
                imageProductService.deleteImageById(image.getId());
            } else {
                product.getImages().remove(image);
            }
        }

        productService.deleteProductById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    public void calculateProductDiscountedPrice(Product product) {
        AtomicReference<Integer> totalDiscount = new AtomicReference<>(0);
        product.getPromotions().forEach(promotion -> {
            System.out.println(promotion.getType());
            if(promotion.getDiscount() != null && !Objects.equals(promotion.getType(), "Mix&Match"))
                totalDiscount.updateAndGet(v -> v + promotion.getDiscount());
        });

        Float discountedPrice = product.getPrice() - product.getPrice() * totalDiscount.get() / 100;
        product.setDiscountedPrice(discountedPrice);
    }

    public void productsRetrieveImages(Set<Product> productsResponse) {
        productsResponse.forEach(this::retrieveProductImages);
    }

    public void retrieveProductImages(Product product){
        final Optional<List<ImageProduct>> retrievedImages = imageProductService.findByProductId(product.getId());

        List<ImageProduct> images = new ArrayList<ImageProduct>();

        if (retrievedImages != null) { retrievedImages.stream().forEach(imgs -> {
            imgs.forEach(image -> {
                ImageProduct img = new ImageProduct(image.getName(), image.getType(),
                        UtilityClass.decompressBytes(image.getPicByte()));
                images.add(img);
            });

        }); }

        product.setImages(images);
    }
}
