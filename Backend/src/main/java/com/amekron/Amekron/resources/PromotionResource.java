package com.amekron.Amekron.resources;
import com.amekron.Amekron.models.Product;
import com.amekron.Amekron.models.Promotion;
import com.amekron.Amekron.services.PromotionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/promotion")
public class PromotionResource {
    private final PromotionService promotionService;

    public PromotionResource(PromotionService promotionService) {
        this.promotionService = promotionService;
    }

    @PostMapping("/add")
    public ResponseEntity<Promotion> addPromotion(@RequestBody Promotion promotion) {
        Promotion newPromotion = promotionService.addPromotion(promotion);
        return new ResponseEntity<>(newPromotion, HttpStatus.CREATED);
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<Promotion>> getAllPromotions() {
        List<Promotion> promotionsResponse = promotionService.getAllPromotions();
        return new ResponseEntity<>(promotionsResponse, HttpStatus.OK);
    }

    @PostMapping("/getById/{id}")
    public ResponseEntity<Promotion> getPromotionById (@PathVariable("id") Long id) {
        Promotion promotionResponse = promotionService.getPromotionById(id);
        return new ResponseEntity<>(promotionResponse, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<Promotion> updatePromotion(@RequestBody Promotion promotion) {
       Promotion updatePromotion = promotionService.updatePromotion(promotion);
        return new ResponseEntity<>(updatePromotion, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deletePromotion(@PathVariable("id") Long id) {
        Promotion promotion = promotionService.getPromotionById(id);
        for (Product product : promotion.getProducts()) {
            product.getPromotions().remove(promotion);
        }

        promotionService.deletePromotionById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
