package com.amekron.Amekron.resources;

import com.amekron.Amekron.models.*;
import com.amekron.Amekron.services.CartService;
import com.amekron.Amekron.services.ImageProductService;
import com.amekron.Amekron.services.ProductService;
import com.amekron.Amekron.services.ReviewService;
import com.amekron.Amekron.utilities.UtilityClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@RestController
@RequestMapping("/cart")
public class CartResource {
    @Autowired
    private CartService cartService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ReviewService reviewService;
    @Autowired
    private ImageProductService imageProductService;

    @PostMapping("/add/{cartId}")
    public ResponseEntity<Cart> addData(@RequestBody Product product, @PathVariable("cartId") Long cartId) {
        Cart cart = cartService.getCartById(cartId);

        if(cart == null){
            cart = new Cart();
            cart.setMixMatch(false);
        }

        //update cart with new product
        List<Product> cartProducts = cart.getProducts();
        if(cartProducts == null) {
            cartProducts = new ArrayList<>();
        }

        cartProducts.add(product);
        cart.setProducts(cartProducts);

        //update quantities
        Map<Long, Integer> cartQuantities = cart.getQuantities();
        if(cartQuantities == null) {
            cartQuantities = new HashMap<>();
        }


        cartQuantities.put(product.getId(), 1);
        cart.setQuantities(cartQuantities);

        //calculate total price of newly added product (after discount)
        Float productTotalPrice;
        if(product.getDiscountedPrice() == null)
            productTotalPrice = product.getPrice();
        else {
            productTotalPrice = product.getDiscountedPrice();
        }

        Map<Long, Float> cartProductTotalPrice = cart.getProductsTotalPrice();
        if(cartProductTotalPrice == null) {
            cartProductTotalPrice = new HashMap<>();
        }

        cartProductTotalPrice.put(product.getId(), productTotalPrice);
        cart.setProductsTotalPrice(cartProductTotalPrice);

        //calculate subtotal new price
        Float subtotal = 0.0f;
        for (Map.Entry<Long, Float> entry : cart.getProductsTotalPrice().entrySet()){
            subtotal += entry.getValue();
        }

        //calculate subtotal old price
        AtomicReference<Float> subtotalOld = new AtomicReference<>(0.0f);
        Cart finalCart = cart;
        cart.getProducts().forEach(p -> {
            subtotalOld.updateAndGet(v -> v + finalCart.getQuantities().get(p.getId()) * p.getPrice());
        });

        cart.setSubtotalPrice(subtotalOld.get());

        //calculate discount in percentage
        AtomicReference<Integer> numberMixMatchInCart = new AtomicReference<>(0);
        AtomicReference<Integer> discountMixMatch = new AtomicReference<>();

        cart.getProducts().forEach(p -> {
            p.getPromotions().forEach(promo -> {
                if(Objects.equals(promo.getType(), "Mix&Match")) {
                    if(promo.getDiscount() != null) {
                        numberMixMatchInCart.getAndSet(numberMixMatchInCart.get() + 1);
                        discountMixMatch.set(promo.getDiscount());
                    }
                }
            });
        });

        //add delivery tax
        cart.setDeliveryTax(10.0f);

        //calculate finalPrice
        //subtotal is already calculated with the new price (discount included)
        Float totalPrice = subtotal + cart.getDeliveryTax();
        Float discount = 0.0f;

        if(numberMixMatchInCart.get() >= 3 || cart.getMixMatch()){
            cart.setMixMatch(true);
            discount = totalPrice * discountMixMatch.get()/100;
            totalPrice -= discount;
        }

        cart.setTotalPrice(totalPrice);

        //calculate discount in float
        discount += subtotalOld.get() - subtotal;
        cart.setDiscount(discount);

        //save cart and product related relations
        cart = cartService.saveData(cart);
        product.setCart(cart);
        List<Review> productReviews = reviewService.getReviewsByProductId(product.getId());
        product.setReviews(productReviews);
        productService.updateProduct(product);
        productsRetrieveImages(cart.getProducts());

        return new ResponseEntity<>(cart, HttpStatus.CREATED);
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<Cart> getCartById (@PathVariable("id") Long id) {
        Cart cartResponse = cartService.getCartById(id);
        productsRetrieveImages(cartResponse.getProducts());
        return new ResponseEntity<>(cartResponse, HttpStatus.OK);
    }

    @PutMapping("/update/{cartId}/{qty}")
    public ResponseEntity<Cart> updateCart(@RequestBody Product product, @PathVariable("cartId") Long cartId, @PathVariable("qty") Integer qty) {
        Cart cart = cartService.getCartById(cartId);

        //update quantity
        Map<Long, Integer> cartQuantities = cart.getQuantities();
        cartQuantities.put(product.getId(), qty);
        cart.setQuantities(cartQuantities);

        //recalculate product total price, based on new qty
        Float productTotalPrice;
        if(product.getDiscountedPrice() == null)
            productTotalPrice = qty * product.getPrice();
        else {
            productTotalPrice = qty * product.getDiscountedPrice();
        }

        Map<Long, Float> cartProductTotalPrice = cart.getProductsTotalPrice();
        cartProductTotalPrice.put(product.getId(), productTotalPrice);
        cart.setProductsTotalPrice(cartProductTotalPrice);

        //recalculate subtotal new price
        Float subtotal = 0.0f;
        for (Map.Entry<Long, Float> entry : cart.getProductsTotalPrice().entrySet()){
            subtotal += entry.getValue();
        }

        //calculate subtotal old price
        AtomicReference<Float> subtotalOld = new AtomicReference<>(0.0f);
        Cart finalCart = cart;
        cart.getProducts().forEach(p -> {
            subtotalOld.updateAndGet(v -> v + finalCart.getQuantities().get(p.getId()) * p.getPrice());
        });

        cart.setSubtotalPrice(subtotalOld.get());

        //get mix match discount
        AtomicReference<Integer> discountMixMatch = new AtomicReference<>();

        cart.getProducts().forEach(p -> {
            p.getPromotions().forEach(promo -> {
                if(Objects.equals(promo.getType(), "Mix&Match")) {
                    if(promo.getDiscount() != null) {
                        discountMixMatch.set(promo.getDiscount());
                    }
                }
            });
        });

        //calculate finalPrice
        //subtotal is already calculated with the new price (discount included)
        Float totalPrice = subtotal + cart.getDeliveryTax();
        Float discount = 0.0f;

        if(cart.getMixMatch() == true){
            discount = totalPrice * discountMixMatch.get()/100;
            totalPrice -= discount;
        }


        cart.setTotalPrice(totalPrice);

        //calculate discount in float
        discount += subtotalOld.get() - subtotal;
        cart.setDiscount(discount);

        //save cart and product related relations
        cart = cartService.saveData(cart);
        product.setCart(cart);
        List<Review> productReviews = reviewService.getReviewsByProductId(product.getId());
        product.setReviews(productReviews);
        productService.updateProduct(product);
        productsRetrieveImages(cart.getProducts());

        return new ResponseEntity<>(cart, HttpStatus.OK);
    }


    @DeleteMapping("/delete/{cartId}/{productId}")
    public ResponseEntity<Cart> deleteCartProduct(@PathVariable("cartId") Long cartId, @PathVariable("productId") Long productId) {
        Cart cart = cartService.getCartById(cartId);
        Product product = productService.getProductById(productId);

        //delete product
        product.setCart(null);
        productService.updateProduct(product);
        cart.getProducts().remove(product);

        //delete qty
        Map<Long, Integer> cartQuantities = cart.getQuantities();
        cartQuantities.remove(productId);
        cart.setQuantities(cartQuantities);

        //delete product subtotal
        Map<Long, Float> cartProductTotalPrice = cart.getProductsTotalPrice();
        cartProductTotalPrice.remove(productId);
        cart.setProductsTotalPrice(cartProductTotalPrice);

        //calculate subtotal new price
        Float subtotal = 0.0f;
        for (Map.Entry<Long, Float> entry : cart.getProductsTotalPrice().entrySet()){
            subtotal += entry.getValue();
        }


        //calculate subtotal old price
        AtomicReference<Float> subtotalOld = new AtomicReference<>(0.0f);
        Cart finalCart = cart;
        cart.getProducts().forEach(p -> {
            subtotalOld.updateAndGet(v -> v + finalCart.getQuantities().get(p.getId()) * p.getPrice());
        });

        cart.setSubtotalPrice(subtotalOld.get());

        //calculate discount in percentage
        AtomicReference<Integer> numberMixMatchInCart = new AtomicReference<>(0);
        AtomicReference<Integer> discountMixMatch = new AtomicReference<>();

        cart.getProducts().forEach(p -> {
            p.getPromotions().forEach(promo -> {
                if(Objects.equals(promo.getType(), "Mix&Match")) {
                    if(promo.getDiscount() != null) {
                        numberMixMatchInCart.getAndSet(numberMixMatchInCart.get() + 1);
                        discountMixMatch.set(promo.getDiscount());
                    }
                }
            });
        });

        //calculate finalPrice
        Float totalPrice = subtotal + cart.getDeliveryTax();
        Float discount = 0.0f;

        if(numberMixMatchInCart.get() >= 3){
            cart.setMixMatch(true);
            discount = totalPrice * discountMixMatch.get()/100;
            totalPrice -= discount;
        } else {
            cart.setMixMatch(false);
        }

        cart.setTotalPrice(totalPrice);

        //calculate discount in float
        discount += subtotalOld.get() - subtotal;
        cart.setDiscount(discount);

        //save cart and product related relations
        cart = cartService.saveData(cart);
        productsRetrieveImages(cart.getProducts());

        return new ResponseEntity<>(cart, HttpStatus.OK);
    }

    public void productsRetrieveImages(List<Product> productsResponse) {
        productsResponse.forEach(this::retrieveProductImages);
    }

    public void retrieveProductImages(Product product){
        final Optional<List<ImageProduct>> retrievedImages = imageProductService.findByProductId(product.getId());

        List<ImageProduct> images = new ArrayList<ImageProduct>();

        if (retrievedImages != null) { retrievedImages.stream().forEach(imgs -> {
            imgs.forEach(image -> {
                ImageProduct img = new ImageProduct(image.getName(), image.getType(),
                        UtilityClass.decompressBytes(image.getPicByte()));
                images.add(img);
            });

        }); }

        product.setImages(images);
    }
}
