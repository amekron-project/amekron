package com.amekron.Amekron.resources;

import com.amekron.Amekron.models.Customer;
import com.amekron.Amekron.models.ImageCustomer;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import com.amekron.Amekron.services.CustomerService;
import com.amekron.Amekron.utilities.ImageCustomerService;
import com.amekron.Amekron.utilities.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(path = "imageCustomer")
public class ImageCustomerResource {
    private final ImageCustomerService imageCustomerService;
    private CustomerService customerService;

    @Autowired
    public ImageCustomerResource(ImageCustomerService imageCustomerService) {
        this.imageCustomerService = imageCustomerService;
    }

    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping("/upload/{customerId}")
    public ResponseEntity<Customer> uploadImage(@RequestParam("imageFile") @NotNull MultipartFile file, @PathVariable("customerId") Long customerId) throws IOException {
        System.out.println("Original Image Byte Size - " + file.getBytes().length);

        Customer customer = this.customerService.getCustomerById(customerId);

        ImageCustomer img = new ImageCustomer(
                file.getOriginalFilename(),
                file.getContentType(),
                UtilityClass.compressBytes(file.getBytes()),
                customer);

        if(customer.getImageCustomer() != null) {
            customer.setImageCustomer(null);
            imageCustomerService.deleteImage(customerId);
        }

        customer.setImageCustomer(img);
        img.setCustomer(customer);
        imageCustomerService.uploadImage(img);
        retrieveImage(customer);

        return new ResponseEntity<>(customer, HttpStatus.OK);
    }

    private void retrieveImage(Customer customer){
        //a customer can only have one image
        final Optional<ImageCustomer> retrievedImage = imageCustomerService.findByCustomerId(customer.getId());

        AtomicReference<ImageCustomer> imageCustomer = new AtomicReference<>();
        if (retrievedImage != null) {
            retrievedImage.stream().forEach(imageFile -> {
                imageCustomer.set(
                        new ImageCustomer(
                                imageFile.getName(),
                                imageFile.getType(),
                                UtilityClass.decompressBytes(imageFile.getPicByte())
                        )
                );
            });
        };

        customer.setImageCustomer(imageCustomer.get());
    }

}
