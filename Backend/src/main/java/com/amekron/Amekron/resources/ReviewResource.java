package com.amekron.Amekron.resources;

import com.amekron.Amekron.models.Customer;
import com.amekron.Amekron.models.ImageCustomer;
import com.amekron.Amekron.models.Product;
import com.amekron.Amekron.models.Review;
import com.amekron.Amekron.services.CustomerService;
import com.amekron.Amekron.utilities.ImageCustomerService;
import com.amekron.Amekron.services.ProductService;
import com.amekron.Amekron.services.ReviewService;
import com.amekron.Amekron.utilities.UtilityClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@RestController
@RequestMapping("/review")
public class ReviewResource {
    @Autowired
    private CustomerService customerService;
    @Autowired
    private ImageCustomerService imageCustomerService;
    @Autowired
    private ReviewService reviewService;
    @Autowired
    private ProductService productService;

    @PostMapping("/add")
    public ResponseEntity<Review> addReview(@RequestBody Review review) {
        reviewService.addReview(review);
        calculateProductRating(review);

        return new ResponseEntity<>(review, HttpStatus.OK);
    }

    @GetMapping("/getById/{id}")
    public ResponseEntity<Review> getById (@PathVariable("id") Long id) {
        Review review = reviewService.getReviewById(id);
        return new ResponseEntity<>(review, HttpStatus.OK);
    }

    @GetMapping("/getByProductId/{id}")
    public ResponseEntity<List<Review>> getReviewsByProductId (@PathVariable("id") Long id) {
        List<Review> reviewsResponse = reviewService.getReviewsByProductId(id);
        reviewsRetrieveCustomerImages(reviewsResponse);
        return new ResponseEntity<>(reviewsResponse, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<List<Review>> deleteReview (@PathVariable("id") Long id) {
        Review review = reviewService.getReviewById(id);
        Product product = productService.getProductById(review.getProduct().getId());

        reviewService.deleteReview(review);

        List<Review> reviewsResponse = reviewService.getReviewsByProductId(review.getProduct().getId());

        if(reviewsResponse.size() > 0)
            calculateProductRating(reviewsResponse.get(0));
        else {
            product.setRating(null);
            productService.updateProduct(product);
        }

        reviewsRetrieveCustomerImages(reviewsResponse);

        return new ResponseEntity<>(reviewsResponse, HttpStatus.OK);
    }

    public void calculateProductRating(Review review) {
        Product product = productService.getProductById(review.getProduct().getId());

        AtomicReference<Integer> overallRating = new AtomicReference<>(0);
        product.getReviews().forEach(r -> {
            overallRating.updateAndGet(v -> v + r.getRating());
        });

        overallRating.set(overallRating.get() / product.getReviews().size());

        product.setRating(overallRating.get());

        productService.updateProduct(product);
    }

    public void reviewsRetrieveCustomerImages(List<Review> reviewsResponse) {
        reviewsResponse.forEach(review -> {
            if(review.getCustomer() != null && review.getCustomer().getDecompressedImageReview() == null){
                retrieveCustomerImage(review.getCustomer(), review.getCustomer().getImageCustomer());
                review.getCustomer().setDecompressedImageReview(true);
            }
        });
    }

    public void retrieveCustomerImage(Customer customer, ImageCustomer imageCustomer){
        //a customer can only have one image

        ImageCustomer decompressedImageCustomer;
        if(imageCustomer != null) {
            decompressedImageCustomer = new ImageCustomer(
                                imageCustomer.getName(),
                                imageCustomer.getType(),
                                UtilityClass.decompressBytes(imageCustomer.getPicByte())
                        );

            System.out.println(imageCustomer.getName());
            customer.setImageCustomer(decompressedImageCustomer);
        }
    }

}
