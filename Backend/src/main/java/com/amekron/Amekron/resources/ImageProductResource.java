package com.amekron.Amekron.resources;
import com.amekron.Amekron.models.ImageProduct;
import com.amekron.Amekron.models.Product;
import com.amekron.Amekron.services.ImageProductService;
import com.amekron.Amekron.services.ProductService;
import com.amekron.Amekron.utilities.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.Deflater;

@RestController
@RequestMapping(path = "imageProduct")
public class ImageProductResource {
    private final ImageProductService imageProductService;
    private ProductService productService;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    public ImageProductResource(ImageProductService imageProductService) {
        this.imageProductService = imageProductService;
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteImage(@PathVariable("id") Long id) {
        imageProductService.deleteImageById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/upload/{productId}")
    public ResponseEntity<?> uploadImage(@RequestParam("imageFile") @NotNull MultipartFile file, @PathVariable("productId") Long productId) throws IOException {
        System.out.println("Original Image Byte Size - " + file.getBytes().length);

        Product product = this.productService.getProductById(productId);

        ImageProduct img = new ImageProduct(file.getOriginalFilename(), file.getContentType(),
                UtilityClass.compressBytes(file.getBytes()), product);
        imageProductService.uploadImage(img);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
