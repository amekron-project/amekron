package com.amekron.Amekron.services;

import com.amekron.Amekron.models.Cart;
import com.amekron.Amekron.repos.CartRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CartService {

    private final CartRepo cartRepo;

    @Autowired
    public CartService(CartRepo cartRepo) {
        this.cartRepo = cartRepo;
    }

    public Cart getCartById(Long id) {
        return cartRepo.getCartById(id);
    }

    public Cart saveData(Cart cart) {
        return cartRepo.save(cart);
    }

    public void deleteCartById(Long id){
        cartRepo.deleteCartById(id);
    }
}
