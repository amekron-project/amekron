package com.amekron.Amekron.services;

import com.amekron.Amekron.models.ImageProduct;
import com.amekron.Amekron.repos.ImageProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class ImageProductService {
    private final ImageProductRepo imageProductRepo;

    @Autowired
    public ImageProductService(ImageProductRepo imageProductRepo) {
        this.imageProductRepo = imageProductRepo;
    }

    public ImageProduct uploadImage(ImageProduct imagesProduct) { return imageProductRepo.save(imagesProduct); }

    public Optional<List<ImageProduct>> findByProductId(Long id) {
        return imageProductRepo.findByProductId(id);
    }

    public void deleteImageById(Long id){
        imageProductRepo.deleteImageById(id);
    }

}
