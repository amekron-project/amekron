package com.amekron.Amekron.services;

import com.amekron.Amekron.exceptions.ObjectNotFoundException;
import com.amekron.Amekron.models.Product;
import com.amekron.Amekron.repos.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class ProductService {
    private final ProductRepo productRepo;

    @Autowired
    public ProductService(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }

    public Product addProduct(Product product) {
        return productRepo.save(product);
    }

    public Product updateProduct(Product product) {
        return productRepo.save(product);
    }

    public void deleteProductById(Long id){
        productRepo.deleteProductById(id);
    }

    public Set<Product> getAllProducts() {
        return productRepo.getAllProducts()
                .orElseThrow(() -> new ObjectNotFoundException("There are no products in the database!"));
    }

    public List<Product> getProductsByCategory(String category) {
        return productRepo.getProductsByCategory(category)
                .orElseThrow(() -> new ObjectNotFoundException("No products found in category " + category));
    }

    public List<Product> getProductsByPromotion(String promotionName) {
        return productRepo.getProductsByPromotion(promotionName)
                .orElseThrow(() -> new ObjectNotFoundException("No products with promotion " + promotionName + " were found!"));

    }

    public Set<Product> getProductsByName(String name) {
        String output = name.substring(0, 1).toUpperCase() + name.substring(1);
        return productRepo.getProductsByName(output)
                .orElseThrow(() -> new ObjectNotFoundException("No products containing " + name + " were found!"));
    }

    public Product getProductById(Long id) {
        return productRepo.getProductById(id)
                .orElseThrow(() -> new ObjectNotFoundException("No product with id " + id + " was found!"));
    }
}
