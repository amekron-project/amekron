package com.amekron.Amekron.services;

import com.amekron.Amekron.exceptions.ObjectNotFoundException;
import com.amekron.Amekron.models.Review;
import com.amekron.Amekron.repos.ReviewRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ReviewService {
    private final ReviewRepo reviewRepo;

    @Autowired
    public ReviewService(ReviewRepo reviewRepo) {
        this.reviewRepo = reviewRepo;
    }

    public void addReview(Review review) {
        reviewRepo.save(review);
    }

    public Review getReviewById(Long id) {
        return reviewRepo.getReviewById(id)
                .orElseThrow(() -> new ObjectNotFoundException("There are no reviews with id " + id));
    }

    public List<Review> getReviewsByProductId(Long id) {
        return reviewRepo.getByProductById(id)
                .orElseThrow(() -> new ObjectNotFoundException("There are no reviews with product id " + id));
    }

    public void deleteReview(Review review) {
        reviewRepo.delete(review);
    }
}
