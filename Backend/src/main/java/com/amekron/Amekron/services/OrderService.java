package com.amekron.Amekron.services;

import com.amekron.Amekron.exceptions.ObjectNotFoundException;
import com.amekron.Amekron.models.Order;
import com.amekron.Amekron.models.Review;
import com.amekron.Amekron.repos.OrderRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {
    private final OrderRepo orderRepo;

    @Autowired
    public OrderService(OrderRepo orderRepo) {
        this.orderRepo = orderRepo;
    }

    public Order addOrder(Order order) {
        return orderRepo.save(order);
    }

    public List<Order> getOrdersByCustomerId(Long id) {
        return orderRepo.getByCustomerById(id)
                .orElseThrow(() -> new ObjectNotFoundException("There are no orders with customer id " + id));
    }
}
