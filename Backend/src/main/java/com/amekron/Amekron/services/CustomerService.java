package com.amekron.Amekron.services;

import com.amekron.Amekron.exceptions.ObjectNotFoundException;
import com.amekron.Amekron.jwt.JwtRequest;
import com.amekron.Amekron.models.Customer;
import com.amekron.Amekron.models.CustomerRole;
import com.amekron.Amekron.repos.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CustomerService implements UserDetailsService {
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private CustomerRepo customerRepo;


    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Customer customer = customerRepo.getCustomerByEmail(email).get();

        List<CustomerRole> customerRoles = customer.getCustomerRoles().stream().collect(Collectors.toList());

        List<GrantedAuthority> grantedAuthorities = customerRoles.stream().map(r -> {
            return new SimpleGrantedAuthority(r.getRole());
        }).collect(Collectors.toList());

        return new org.springframework.security.core.userdetails.User(email, customer.getPassword(), grantedAuthorities);
    }

    public Customer saveCustomer(JwtRequest jwtRequest) throws RuntimeException{
        if(customerRepo.getCustomerByEmail(jwtRequest.getEmail()).isPresent())
            throw new RuntimeException("User already exists");

        Customer customer = new Customer();
        customer.setFirstName(jwtRequest.getFirstName());
        customer.setLastName(jwtRequest.getLastName());
        customer.setEmail(jwtRequest.getEmail());
        customer.setAddress(jwtRequest.getAddress());
        customer.setCity(jwtRequest.getCity());
        customer.setCountry(jwtRequest.getCountry());
        customer.setZipCode(jwtRequest.getZipCode());
        customer.setPhone(jwtRequest.getPhone());

        customer.setPassword(passwordEncoder.encode(jwtRequest.getPassword()));

        customer.setCustomerRoles(jwtRequest.getRoles().stream().map(role -> {
            CustomerRole customerRole = new CustomerRole();
            customerRole.setRole(role);
            return customerRole;
        }).collect(Collectors.toSet()));

        return customerRepo.save(customer);
    }

    public Customer addCustomer(Customer customer) {
        return customerRepo.save(customer);
    }

    public Customer updateCustomer(Customer customer) { return customerRepo.save(customer); }

    public void deleteCustomer(Long id){
        customerRepo.deleteCustomerById(id);
    }


    public Customer getCustomerById(Long id) {
        return customerRepo.getCustomerById(id)
                .orElseThrow(() -> new ObjectNotFoundException("Customer with id " + id + " was not found"));

    }

    public Customer getCustomerByEmailAndPassword(String email, String password) {
        return customerRepo.getCustomerByEmailAndPassword(email, password)
                .orElseThrow(() -> new ObjectNotFoundException("Customer by email and password " + email + " " + password + " was not found"));
    }

    public Customer getCustomerByEmail(String email) {
        return customerRepo.getCustomerByEmail(email)
                .orElseThrow(() -> new ObjectNotFoundException("Customer by email " + email + " was not found"));

    }
}
