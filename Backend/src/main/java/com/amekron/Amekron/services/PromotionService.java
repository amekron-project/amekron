package com.amekron.Amekron.services;

import com.amekron.Amekron.exceptions.ObjectNotFoundException;
import com.amekron.Amekron.models.Product;
import com.amekron.Amekron.models.Promotion;
import com.amekron.Amekron.repos.PromotionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class PromotionService {
    private final PromotionRepo promotionRepo;

    @Autowired
    public PromotionService(PromotionRepo promotionRepo) {
        this.promotionRepo = promotionRepo;
    }

    public Promotion addPromotion(Promotion promotion) {
        return promotionRepo.save(promotion);
    }

    public List<Promotion> getAllPromotions() {
        return promotionRepo.getAllPromotions()
                .orElseThrow(() -> new ObjectNotFoundException("There are no promotions in the database!"));
    }

    public Promotion getPromotionById(Long id) {
        return promotionRepo.getPromotionById(id)
                .orElseThrow(() -> new ObjectNotFoundException("No promotion with id " + id + " was found!"));
    }

    public Promotion updatePromotion(Promotion promotion) { return promotionRepo.save(promotion); }

    public void deletePromotionById(Long id){
        promotionRepo.deletePromotionById(id);
    }
}
