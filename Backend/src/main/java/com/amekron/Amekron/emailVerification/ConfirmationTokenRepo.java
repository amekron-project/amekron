package com.amekron.Amekron.emailVerification;

import org.springframework.data.repository.CrudRepository;

public interface ConfirmationTokenRepo extends CrudRepository<ConfirmationToken, String> {
    ConfirmationToken findByConfirmationToken(String confirmationToken);
}
