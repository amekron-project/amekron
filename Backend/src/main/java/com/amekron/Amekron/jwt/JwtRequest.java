package com.amekron.Amekron.jwt;

import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Getter
@Setter
public class JwtRequest {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String address;
    private String city;
    private String country;
    private String zipCode;
    private String phone;
    private List<String> roles;
}
