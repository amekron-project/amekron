package com.amekron.Amekron.jwt;

import com.amekron.Amekron.models.Customer;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class JwtResponse {
    private Customer customer;
    private String token;
    private List<String> roles;

}
