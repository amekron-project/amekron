package com.amekron.Amekron.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PromotionCategoryObj {
    private List<String> selectedPromotions;
    private List<String> selectedCategories;
}
