package com.amekron.Amekron.models;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "image_customer")
@NoArgsConstructor
@Getter
@Setter
public class ImageCustomer implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(optional = false)
    @MapsId
    @JoinColumn(name="customer_id")
    @JsonIgnore
    private Customer customer;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private String type;

    //image bytes can have large lengths so we specify a value
    //which is more than the default length for picByte column
    @Column(name = "picByte", length = 1000)
    private byte[] picByte;

    public ImageCustomer(String name, String type, byte[] picByte, Customer customer) {
        this.customer = customer;
        this.name = name;
        this.type = type;
        this.picByte = picByte;
    }

    public ImageCustomer(String name, String type, byte[] picByte) {
        this.name = name;
        this.type = type;
        this.picByte = picByte;
    }
}
