package com.amekron.Amekron.models;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Entity
@Table
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class Cart implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false, name = "cart_id")
    private Long id;

    @OneToMany(mappedBy = "cart")
    private List<Product> products;

    @Column(nullable = false)
    @ElementCollection
    @CollectionTable(name = "cart_quantities", joinColumns = @JoinColumn(name = "cart_id"))
    private Map<Long, Integer> quantities;

    @Column(nullable = false)
    @ElementCollection
    @CollectionTable(name = "cart_products_total_price", joinColumns = @JoinColumn(name = "cart_id"))
    private Map<Long, Float> productsTotalPrice;

    @Column(nullable = false)
    private Float subtotalPrice;

    @Column
    private Float discount;

    @Column
    private Float deliveryTax;

    @Column(nullable = false)
    private Float totalPrice;

    @Column
    private Boolean mixMatch;

    @OneToOne(mappedBy = "cart")
    @JsonIgnore
    private Order order;

    public void setOrder(Order order) {
        this.order = order;

        if(order != null)
            order.setCart(this);
    }

    public void setDeliveryTax(Float deliveryTax) {
        this.deliveryTax = deliveryTax;
    }

    public void setQuantities(Map<Long, Integer> quantities) {
        this.quantities = quantities;
    }

    public void setProductsTotalPrice(Map<Long, Float> productsTotalPrice) {
        this.productsTotalPrice = productsTotalPrice;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setSubtotalPrice(Float subtotalPrice) {
        this.subtotalPrice = subtotalPrice;
    }

    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public void setTotalPrice(Float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public void setMixMatch(Boolean mixMatch) {
        this.mixMatch = mixMatch;
    }
}
