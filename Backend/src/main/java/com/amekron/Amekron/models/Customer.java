package com.amekron.Amekron.models;
import com.amekron.Amekron.emailVerification.ConfirmationToken;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
@Table
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class Customer implements Serializable {
    private Boolean decompressedImageReview;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false, name = "customer_id")
    private Long id;
    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String lastName;
    @Column(nullable = false, updatable = false)
    private String email;
    @Column(nullable = false)
    private String password;
    @Column
    private String address;
    @Column
    private String city;
    @Column
    private String country;
    @Column
    private String zipCode;
    @Column
    private String phone;
    @Column
    private Boolean isEnabled;
    @OneToOne(mappedBy = "customer", orphanRemoval = true, cascade = CascadeType.ALL)
    private ImageCustomer imageCustomer;
    @OneToOne(mappedBy = "customer", orphanRemoval = true, cascade = CascadeType.ALL)
    @JsonIgnore
    private ConfirmationToken confirmationToken;
    @OneToMany(mappedBy = "customer", orphanRemoval = true)
    @JsonIgnore
    private List<Review> reviews;
    @OneToMany(mappedBy = "customer", orphanRemoval = true, cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Order> orders;

    //for jwt
    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<CustomerRole> customerRoles;

    public void setCustomerRoles(Set<CustomerRole> customerRoles) {
        this.customerRoles = customerRoles;

        for (CustomerRole r : customerRoles) {
            r.setCustomer(this);
        }
    }

    public void setIsEnabled(Boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public void setConfirmationToken(ConfirmationToken confirmationToken) {
        this.confirmationToken = confirmationToken;
    }

    public void setDecompressedImageReview(Boolean decompressedImageReview) {
        this.decompressedImageReview = decompressedImageReview;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setImageCustomer(ImageCustomer imageCustomer) {
        this.imageCustomer = imageCustomer;

        if(imageCustomer != null)
        imageCustomer.setCustomer(this);
    }
}
