package com.amekron.Amekron.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Promotion implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private Date start_time;
    @Column(nullable = false)
    private Date end_time;
    @Column()
    private String description;
    @Column()
    private String type;
    @Column()
    private Integer discount;

    @JsonIgnore
    @ManyToMany(mappedBy = "promotions")
    private List<Product> products;
}
