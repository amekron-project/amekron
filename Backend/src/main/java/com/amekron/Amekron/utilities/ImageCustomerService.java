package com.amekron.Amekron.utilities;

import com.amekron.Amekron.models.ImageCustomer;
import com.amekron.Amekron.repos.ImageCustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Transactional
@Service
public class ImageCustomerService {
    private final ImageCustomerRepo imageCustomerRepo;

    @Autowired
    public ImageCustomerService(ImageCustomerRepo imageCustomerRepo) {
        this.imageCustomerRepo = imageCustomerRepo;
    }

    public void deleteImage(Long id){
        imageCustomerRepo.deleteImageByCustomerId(id);
    }

    public ImageCustomer uploadImage(ImageCustomer imageCustomer) { return imageCustomerRepo.save(imageCustomer); }

    public Optional<ImageCustomer> findByCustomerId(Long customerId) {
        return imageCustomerRepo.findByCustomerId(customerId);
    }
}
